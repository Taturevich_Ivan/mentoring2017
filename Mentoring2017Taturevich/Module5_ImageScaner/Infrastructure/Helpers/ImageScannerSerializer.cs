﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Module5_ImageScaner.Infrastructure.ReadModels;

namespace Module5_ImageScaner.Infrastructure.Helpers
{
    public static class ImageScannerSerializer
    {
        private static string GetPathToExecutorSettings(string executorMainClassLocation)
        {
            var str = Path.GetFullPath(Path.Combine(executorMainClassLocation,
                @"..\bin\Debug\imageScannerSettings.xml"));
            if (!File.Exists(str))
            {
                str = Path.GetFullPath(Path.Combine(executorMainClassLocation,
                    @"..\bin\Release\imageScannerSettings.xml"));
            }

            return str;
        }

        private static string GetPathToSettings(string relatedMainClassLocation)
        {
            var str = Path.GetFullPath(Path.Combine(relatedMainClassLocation,
                @"..\..\Module5_ImageScaner\imageScannerSettings.xml"));
            return str;
        }

        public static void SerializeObjectAndSaveInFile(ImageScannerOptions options, [CallerFilePath] string saveLocationPath = "")
        {
            var serializer = new XmlSerializer(typeof(ImageScannerOptions));
            var stringWriter = new StringWriter();
            var finalPath = GetPathToSettings(saveLocationPath);
            using (new XmlTextWriter(stringWriter)
            {
                Formatting = Formatting.Indented,
            })
            {
                serializer.Serialize(stringWriter, options);
                var xdoc = new XmlDocument();
                xdoc.LoadXml(stringWriter.ToString());
                xdoc.Save("imageScannerSettings.xml");
                var localSettingsPath = GetPathToExecutorSettings(saveLocationPath);
                FileHelper.MoveFilewithOverWrite(localSettingsPath, finalPath);
            }
        }

        public static ImageScannerOptions DeserializeObject([CallerFilePath] string saveLocationPath = "")
        {
            var result = new ImageScannerOptions();
            var pathTosettings = GetPathToSettings(saveLocationPath);
            if (!string.IsNullOrEmpty(pathTosettings))
            {
                var serializer = new XmlSerializer(typeof(ImageScannerOptions));
                using (var reader = new StreamReader(pathTosettings))
                {
                    result = (ImageScannerOptions)serializer.Deserialize(reader);
                }
            }

            return result;
        }

        public static byte[] SerializeStatusMessageToBytesViaXml(FileWatcherStatusMessage message)
        {
            var serializer = new XmlSerializer(typeof(FileWatcherStatusMessage));
            var stringWriter = new StringWriter();
            using (new XmlTextWriter(stringWriter)
            {
                Formatting = Formatting.Indented,
            })
            {
                serializer.Serialize(stringWriter, message);
                var xdoc = new XmlDocument();
                xdoc.LoadXml(stringWriter.ToString());
                return Encoding.UTF8.GetBytes(xdoc.OuterXml);
            }
        }

        public static FileWatcherStatusMessage DeserialiseFromBytesToStatus(byte[] bytes)
        {
            var doc = new XmlDocument();
            var xml = Encoding.UTF8.GetString(bytes);
            var serializer = new XmlSerializer(typeof(FileWatcherStatusMessage));
            using (var reader = new StringReader(xml))
            {
                return (FileWatcherStatusMessage)serializer.Deserialize(reader);
            }
        }

        public static void DeserializeStatusIntoXmlAndSaveInFile(byte[] bytes, string fileName)
        {
            List<FileWatcherStatusMessage> currentList;
            var deSerializer = new XmlSerializer(typeof(List<FileWatcherStatusMessage>));
            using (var reader = new StreamReader(fileName))
            {
                currentList = (List<FileWatcherStatusMessage>)deSerializer.Deserialize(reader);
            }

            var newStatus = DeserialiseFromBytesToStatus(bytes);
            currentList.RemoveAll(x => x.MessageId == newStatus.MessageId);
            currentList.Add(newStatus);
            var stringWriter = new StringWriter();
            var serializer = new XmlSerializer(typeof(List<FileWatcherStatusMessage>));
            using (new XmlTextWriter(stringWriter)
            {
                Formatting = Formatting.Indented,
            })
            {
                serializer.Serialize(stringWriter, currentList);
                var xdoc = new XmlDocument();
                xdoc.LoadXml(stringWriter.ToString());
                xdoc.Save(fileName);
            }
        }
    }
}
