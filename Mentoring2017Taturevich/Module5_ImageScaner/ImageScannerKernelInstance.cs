﻿using AOPModule;
using AOPModule.Proxy;
using Module5_ImageScaner.Infrastructure;
using Module5_ImageScaner.Infrastructure.Helpers;
using Module5_ImageScaner.Infrastructure.ReadModels;
using Ninject;
using Ninject.Extensions.Interception.Infrastructure.Language;

namespace Module5_ImageScaner
{
    public static class ImageScannerKernelInstance
    {
        private static readonly StandardKernel Kernel = new StandardKernel(
            new NinjectSettings { LoadExtensions = true });

        static ImageScannerKernelInstance()
        {
            Kernel.Load(new AopModule());
            Kernel.Bind<IBitmapHelper>().To<BitmapHelper>().Intercept().With<SerializationInterceptor>();
            Kernel.Bind<IPdfHelper>().To<PdfHelper>().Intercept().With<SerializationInterceptor>();
            Kernel.Bind<IFileProcessor>().To<FileProcessor>().Intercept().With<SerializationInterceptor>();
            Kernel.Bind<IBarcodeManager>().To<BarcodeManager>().Intercept().With<SerializationInterceptor>();
            Kernel.Bind<IFileSenderMessageService>().To<FileSenderMessageService>().Intercept().With<SerializationInterceptor>();
            Kernel.Bind<ImageScannerOptions>().ToSelf();

        }

        public static T Get<T>() => Kernel.Get<T>();
    }
}
