﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Sample03.E3SClient.Entities;

namespace Sample03.E3SClient
{
    public class FtsRequestGenerator
    {
        private readonly UriTemplate FTSSearchTemplate = new UriTemplate(@"data/searchFts?metaType={metaType}&query={query}&fields={fields}");
        private readonly Uri BaseAddress;

        public FtsRequestGenerator(string baseAddres) : this(new Uri(baseAddres))
        {
        }

        public FtsRequestGenerator(Uri baseAddress)
        {
            BaseAddress = baseAddress;
        }

        public Uri GenerateRequestUrl<T>(string query = "*", int start = 0, int limit = 10)
        {
            return GenerateRequestUrl(typeof(T), query, start, limit);
        }

        public Uri GenerateRequestUrl(Type type, string query = "*", int start = 0, int limit = 10)
        {
            string metaTypeName = GetMetaTypeName(type);

            var queryList = query.Split(QueryConstants.AndSeparator);
            var ftsQueryRequest = new FTSQueryRequest
            {
                Statements = queryList
                    .Select(q => new Statement { Query = q })
                    .ToList(),
                Start = start,
                Limit = limit
            };

            var ftsQueryRequestString = JsonConvert.SerializeObject(ftsQueryRequest);

            var uri = FTSSearchTemplate.BindByName(BaseAddress,
                new Dictionary<string, string>
                {
                    { "metaType", metaTypeName },
                    { "query", ftsQueryRequestString }
                });

            return uri;
        }

        private string GetMetaTypeName(Type type)
        {
            var attributes = type.GetCustomAttributes(typeof(E3SMetaTypeAttribute), false);

            if (attributes.Length == 0)
                throw new Exception($"Entity {type.FullName} do not have attribute E3SMetaType");

            return ((E3SMetaTypeAttribute)attributes[0]).Name;
        }

    }
}
