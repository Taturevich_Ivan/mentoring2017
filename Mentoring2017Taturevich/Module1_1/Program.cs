﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Module1_1.ExpressionVisitors;

namespace Module1_1
{
    static class Program
    {
        static void Main()
        {
            BinaryVisitorDemo();
            LambdaVisitorDemo();

            Console.ReadKey();
        }

        private static void BinaryVisitorDemo()
        {
            var replacedParameters = new Dictionary<string, object>
            {
                { "x", 10 },
                { "y", 11 },
            };
            var binaryVisitor = new ConversionExpressionVisitor(replacedParameters);
            Expression<Func<int, int>> incrementOldExpression = x => x + 1;
            Expression<Func<int, int>> decrementOldExpression = y => y - 1;
            Console.WriteLine($"Expression before replacing: {incrementOldExpression}");
            Console.WriteLine($"Expression before replacing: {decrementOldExpression}");
            var incrementNewExpression = binaryVisitor.Visit(incrementOldExpression);
            var decrementNewExpression = binaryVisitor.Visit(decrementOldExpression);
            Console.WriteLine($"Expression after replacing: {incrementNewExpression}");
            Console.WriteLine($"Expression after replacing: {decrementNewExpression}");
        }

        public static void LambdaVisitorDemo()
        {
            Expression<Func<int, int, int, int>> lambdaExpression = (a, b, c) => (a * b) + c - 13;
            Console.WriteLine($"Expression before replacing: {lambdaExpression}");
            var replacedParameters = new Dictionary<string, object>
            {
                { "a", 10 },
                { "b", 13 },
                { "c", 1 }
            };

            var parameterVisitor = new ConversionExpressionVisitor(replacedParameters);
            var conversionResult = parameterVisitor.Visit(lambdaExpression);
            Console.WriteLine($"Expression after replacing: {conversionResult}");

            // TODO: Try to calculate value from expression. Need advise how to simplify this Compile/Invoke calls
            try
            {
                var endResult = conversionResult.ToLambdaExpression<Func<int>>();
                Console.WriteLine($"Calculation result : {endResult.Compile()()()}");
            }
            catch (Exception)
            {
                Console.WriteLine("Can calculate only expressions without parameters");
            }
        }
    }
}
