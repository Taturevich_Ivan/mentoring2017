﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using PostSharp.Aspects;

namespace AOPModule.Decorators
{
    [Serializable]
    public class LoggingAspect : OnMethodBoundaryAspect
    {
        [NonSerialized]
        private CallerDiagnosticObject _callerDiagmosticObject;

        public override void OnEntry(MethodExecutionArgs args)
        {
            base.OnEntry(args);
            _callerDiagmosticObject = new CallerDiagnosticObject
            {
                Parameters = args
                    .Arguments
                    .Select(x => x.GetType()
                        .ToString())
                    .ToArray(),
                MethodName = args.Method.Name
            };
        }

        public override void OnException(MethodExecutionArgs args)
        {
            base.OnException(args);
            _callerDiagmosticObject.ExceptionMessage = "Error occurs during invocation";
            SaveToFile();
        }

        public override void OnSuccess(MethodExecutionArgs args)
        {
            base.OnSuccess(args);
            _callerDiagmosticObject.ReturnValue = args.ReturnValue;
            _callerDiagmosticObject.CallDate = DateTime.Now.ToShortDateString();
            SaveToFile();
        }

        private void SaveToFile()
        {
            var fileRootPath = Path.GetFullPath(
                Path.Combine(Directory.GetCurrentDirectory(),
                    @"..\..\..\APPLICATION_LOGS_POSTSharp.json"));
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "APPLICATION_LOGS_POSTSharp.json");
            try
            {
                var serializedObject = JsonConvert.SerializeObject(_callerDiagmosticObject);
                File.AppendAllText(filePath, serializedObject);
                File.AppendAllText(fileRootPath, serializedObject);
            }
            catch (Exception)
            {
                File.AppendAllText(filePath, "Not Serializable");
                File.AppendAllText(fileRootPath, "Not Serializable");
            }
        }
    }
}
