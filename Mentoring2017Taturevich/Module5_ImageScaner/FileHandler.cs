﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Module5_ImageScaner.Infrastructure;
using Module5_ImageScaner.Infrastructure.Helpers;
using Module5_ImageScaner.Infrastructure.ReadModels;
using RabbitMQ.Client.Events;

namespace Module5_ImageScaner
{
    public class FileHandler
    {
        private bool _isStarted;
        private readonly string _settingsDirectory;
        private readonly AutoResetEvent _newFileEvent;
        private readonly ManualResetEvent _stopWorkEvent;
        private readonly ManualResetEventSlim _addFileEvent;
        private readonly WatcherManager<string> _watcherManager;
        private readonly IFileProcessor _fileParser = ImageScannerKernelInstance.Get<IFileProcessor>();
        private ImageScannerOptions _imageScannerOptions = ImageScannerKernelInstance.Get<ImageScannerOptions>();
        private readonly IFileSenderMessageService _settingsQueue = ImageScannerKernelInstance.Get<IFileSenderMessageService>();

        public FileHandler([CallerFilePath] string filePath = "")
        {
            var currentDirectory = Path.GetDirectoryName(filePath);
            if (currentDirectory != null)
            {
                _settingsDirectory = Path
                    .Combine(currentDirectory, FileHelper.GlobalSettingsFileName);
            }

            _newFileEvent = new AutoResetEvent(false);
            _stopWorkEvent = new ManualResetEvent(false);
            _addFileEvent = new ManualResetEventSlim(false);
            _settingsQueue.Consumer.Received += ConsumerOnReceived;
            _settingsQueue.StartConsumeSettings();
            _watcherManager = new WatcherManager<string>(ObserverProcess);
        }

        private void SetUpWatchers(string settingDirectory)
        {
            if (settingDirectory != null)
            {
                _imageScannerOptions = ImageScannerSerializer.DeserializeObject(settingDirectory);
                if (_imageScannerOptions != null)
                {
                    CleanUpWatchers(_imageScannerOptions.ObservedFolders);
                    _fileParser.OutputType = _imageScannerOptions.OutputType;
                    _fileParser.BarcodeText = _imageScannerOptions.BarcodeText;
                    _fileParser.OutputDirectory = _imageScannerOptions.OutputFolder;
                    _fileParser.TemporaryDirectory = _imageScannerOptions.TemporaryFolder;
                    _watcherManager.ObservedDirectories
                        .AddRange(_imageScannerOptions.ObservedFolders);
                    foreach (var observedFolder in _imageScannerOptions.ObservedFolders)
                    {
                        var newWatcher = new FileSystemWatcher(observedFolder);
                        newWatcher.Created += NewWatcherOnCreated;
                        newWatcher.EnableRaisingEvents = true;
                        var newMessageService = new FileSenderMessageService();
                        _watcherManager.WorkerWatchers
                            .Add(new FileMessageWatcher(newWatcher, newMessageService)
                            {
                                CurrentStatus = FileMessageWatcherStatus.WaitingForFiles
                            });
                        Console.WriteLine("Added new watcher with queue");
                    }
                }
            }
        }

        private void ConsumerOnReceived(object o, BasicDeliverEventArgs basicDeliverEventArgs)
        {
            var headers = basicDeliverEventArgs.BasicProperties.Headers;
            if (headers.ContainsKey("SettingsChanged") && (bool)headers["SettingsChanged"])
            {
                RestartWatchermanager();
            }
            else
            {
                _settingsQueue.Consumer.Model.BasicNack(basicDeliverEventArgs.DeliveryTag, false, true);
            }
        }

        private void CleanUpWatchers(List<string> observedFolders)
        {
            if (!_watcherManager.ObservedDirectories.SequenceEqual(observedFolders))
            {
                foreach (var workerWatcher in _watcherManager.WorkerWatchers)
                {
                    workerWatcher.Dispose();
                }
                _watcherManager.WorkerWatchers.Clear();
                _watcherManager.ObservedDirectories.Clear();
                Console.WriteLine("Watcher was deleted");
            }
        }

        private void NewWatcherOnCreated(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            _newFileEvent.Set();
            _addFileEvent.Set();
        }

        private void ObserverProcess(string directoryName)
        {
            do
            {
                // Handle stop service event
                if (_stopWorkEvent.WaitOne(TimeSpan.Zero))
                {
                    return;
                }

                // Handle first start processing event
                if (_isStarted)
                {
                    _watcherManager.SetWatcherSettings(
                        directoryName,
                        FileMessageWatcherStatus.ProcessingFiles,
                        _imageScannerOptions);
                    ProcessFilesInDirectory(directoryName);
                    _isStarted = false;
                    _watcherManager.SetWatcherSettings(
                        directoryName,
                        FileMessageWatcherStatus.WaitingForFiles,
                        _imageScannerOptions);
                }

                // Handle added file event
                _addFileEvent.Wait();

                // Handle waiting logic and bulk insertion
                StartFilesWaiter(directoryName);
                _addFileEvent.Reset();
            }
            while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent, _newFileEvent }, 3000) != 0);
        }

        public void Start()
        {
            _isStarted = true;
            SetUpWatchers(_settingsDirectory);
            _watcherManager.StartProcessing();
        }

        public void Stop()
        {
            _isStarted = false;
            _watcherManager.DisableEventRaising();
            _stopWorkEvent.Set();
        }

        private void RestartWatchermanager()
        {
            if (_settingsDirectory != null)
            {
                Stop();
                Start();
            }
        }

        private void StartFilesWaiter(string directoryName)
        {
            _addFileEvent.Reset();
            if (!_addFileEvent.Wait(_imageScannerOptions.DelayTime))
            {
                _watcherManager.SetWatcherSettings(
                    directoryName,
                    FileMessageWatcherStatus.ProcessingFiles,
                    _imageScannerOptions);
                ProcessFilesInDirectory(directoryName);
                _watcherManager.SetWatcherSettings(
                    directoryName,
                    FileMessageWatcherStatus.WaitingForFiles,
                    _imageScannerOptions);
            }
            else if (Directory.GetFiles(directoryName).Length > 1)
            {
                StartFilesWaiter(directoryName);
            }
        }

        private void ProcessFilesInDirectory(string directoryName)
        {
            var filesInDirectory = Directory.GetFiles(directoryName);
            if (filesInDirectory.Any())
            {
                _fileParser.StartProcessingFiles(filesInDirectory.ToList());
            }
        }
    }
}
