﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace Module6_Debugging
{
    static class Program
    {
        static void Main()
        {
            var firstOrDefault = NetworkInterface.GetAllNetworkInterfaces()
                .FirstOrDefault();
            if (firstOrDefault != null)
            {
                var addressBytes = firstOrDefault
                    .GetPhysicalAddress()
                    .GetAddressBytes();
                Console.WriteLine(addressBytes);
            }
            var date = DateTime.Now.Date.ToBinary();
            var dateBytes = BitConverter.GetBytes(date);
            Console.WriteLine(dateBytes);

            var test = new CrackMe.Form1();
            Application.EnableVisualStyles();
            Application.Run(test);
            Console.ReadLine();
        }
    }
}
