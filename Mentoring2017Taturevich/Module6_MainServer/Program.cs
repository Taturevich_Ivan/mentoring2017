﻿using System;
using System.IO;
using System.Text;
using Module5_ImageScaner.Infrastructure;
using Module5_ImageScaner.Infrastructure.Helpers;
using Module5_ImageScaner.Infrastructure.ReadModels;
using Ninject;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Module6_MainServer
{
    static class Program
    {
        private static StandardKernel _kernel = new StandardKernel();

        private static readonly string StatusesFileName
            = Path.GetFullPath(
                Path.Combine(Directory.GetCurrentDirectory(),
                    @"..\..\watcherStatuses.xml"));

        private static readonly FilePostProcessor FilePostProcessor = new FilePostProcessor();

        private static void InitKernel()
        {
            _kernel.Bind<FilePostProcessor>().ToSelf();
            _kernel.Load(new AopModule());
            var fileProcessot = _kernel.Get<FilePostProcessor>();
        }

        static void Main()
        {
            InitKernel();
            var pdfHelper = new PdfHelper();
            var factory = new ConnectionFactory
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost",
                RequestedHeartbeat = 30
            };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "Mentoring2017FileQueue",
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var contentType = ea.BasicProperties.ContentType;
                    var objectType = ea.BasicProperties.Type;

                    if (IsObjectFileStatusSettings(contentType, objectType))
                    {
                        ImageScannerSerializer
                            .DeserializeStatusIntoXmlAndSaveInFile(body, StatusesFileName);
                        Console.WriteLine("Status file was updated");
                    }
                    else if (IsObjectFile(contentType))
                    {
                        HandleFileMessage(body, ea.BasicProperties);
                    }
                };
                channel.BasicConsume("Mentoring2017FileQueue", true, consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }
        }

        private static void HandleFileMessage(byte[] body, IBasicProperties properties)
        {
            var headerModel = ConvertPropertiesIntoHeaders(properties);
            if (headerModel.IsChunked)
            {
                FilePostProcessor.StartPostProcessing(
                    headerModel.FileId,
                    body,
                    headerModel.IsComplete,
                    headerModel.OutputType,
                    headerModel.SaveFilePath);
            }
            else
            {
                FilePostProcessor.SaveByteArrayToFile(body, headerModel.OutputType, headerModel.SaveFilePath);
            }
        }

        private static bool IsObjectFile(string contentType)
        {
            return contentType == "application/octet-stream";
        }

        private static bool IsObjectFileStatusSettings(string contentType, string objectType)
        {
            var isContentTypeXml = contentType == "text/xml";
            var isObjectTypeScannerSettings = objectType == typeof(FileWatcherStatusMessage).Name;

            return isContentTypeXml && isObjectTypeScannerSettings;
        }

        private static HeadersModel ConvertPropertiesIntoHeaders(IBasicProperties properties)
        {
            var resultModel = new HeadersModel();
            var headers = properties.Headers;

            resultModel.FileId = headers.ContainsKey(nameof(HeadersModel.FileId))
                ? ConvertHeaderValue<string>(headers[nameof(HeadersModel.FileId)])
                : Guid.Empty.ToString();

            resultModel.SaveFilePath = headers.ContainsKey(nameof(HeadersModel.SaveFilePath))
                ? ConvertHeaderValue<string>(headers[nameof(HeadersModel.SaveFilePath)])
                : Directory.GetCurrentDirectory();

            resultModel.OutputType = headers.ContainsKey(nameof(HeadersModel.OutputType))
                ? ConvertHeaderValue<OutputType>(headers[nameof(HeadersModel.OutputType)])
                : default(OutputType);

            resultModel.IsChunked = !headers.ContainsKey(nameof(HeadersModel.IsChunked))
                || ConvertHeaderValue<bool>(headers[nameof(HeadersModel.IsChunked)]);

            resultModel.ChunkCount = headers.ContainsKey(nameof(HeadersModel.ChunkCount))
                ? ConvertHeaderValue<int>(headers[nameof(HeadersModel.ChunkCount)])
                : default(int);

            resultModel.IsComplete = !headers.ContainsKey(nameof(HeadersModel.IsComplete))
                || ConvertHeaderValue<bool>(headers[nameof(HeadersModel.IsComplete)]);

            return resultModel;
        }

        private static T ConvertHeaderValue<T>(object objectvalue)
        {
            if (objectvalue is T)
            {
                return (T)objectvalue;
            }

            var bytesValue = (byte[])objectvalue;
            var stringValue = Encoding.UTF8.GetString(bytesValue);
            if (typeof(T).IsEnum)
            {
                return (T)Enum.Parse(typeof(T), stringValue, true);
            }

            return (T)Convert.ChangeType(stringValue, typeof(T));
        }
    }
}
