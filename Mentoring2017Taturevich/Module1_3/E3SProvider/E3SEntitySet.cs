﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Sample03.E3SClient;

namespace Sample03.E3SProvider
{
    public class E3SEntitySet<T> : IQueryable<T>
        where T : E3SEntity
    {
        private readonly Expression _expression;
        private readonly IQueryProvider _provider;

        public E3SEntitySet()
        {
            _expression = Expression.Constant(this);
            var client = new E3SQueryClient();
            _provider = new E3SLinqProvider(client);
        }

        public E3SEntitySet(string user, string password)
        {
            _expression = Expression.Constant(this);
            var client = new E3SQueryClient(user, password);
            _provider = new E3SLinqProvider(client);
        }

        public Type ElementType => typeof(T);

        public Expression Expression => _expression;
        public IQueryProvider Provider => _provider;

        public IEnumerator<T> GetEnumerator()
        {
            return _provider.Execute<IEnumerable<T>>(_expression).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _provider.Execute<IEnumerable>(_expression).GetEnumerator();
        }
    }
}
