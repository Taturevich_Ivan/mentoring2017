﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using Module5_ImageScaner.Infrastructure.Helpers;

namespace Module5_UI
{
    public interface IServiceManager
    {
        Task Run();

        Task Install();

        Task UnInstall();

        Task Stop();

        bool IsServiceExist { get; }

        ServiceControllerStatus? GetServiceControllerStatus();

        void ClearLogs();

        string GetLogInfo();
    }

    internal class ServiceManager : IServiceManager
    {
        private readonly Process _process;
        private readonly string _serviceLogsPath = Path.GetFullPath(
            Path.Combine(Directory.GetCurrentDirectory(),
            @"..\..\..\Module5_ImageScaner\bin\Debug\log.txt"));

        public ServiceManager()
        {
            var serviceInstansePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(),
                @"..\..\..\Module5_ImageScaner\bin\Debug\"));
            _process = new Process
            {
                StartInfo =
                {
                    FileName =
                        @"Module5_ImageScaner.exe",
                    WorkingDirectory = serviceInstansePath,
                    Verb = "runas"
                }
            };
        }

        public async Task Install()
        {
            await ExecuteServiceCommandAsync("install");
        }

        public async Task UnInstall()
        {
            await ExecuteServiceCommandAsync("uninstall");
        }

        public async Task Run()
        {
            await ExecuteServiceCommandAsync("start");
        }

        public async Task Stop()
        {
            await ExecuteServiceCommandAsync("stop");
        }

        public bool IsServiceExist => ServiceController
            .GetServices()
            .Any(x => x.ServiceName == "Mentoring2017Taturevich");

        public ServiceControllerStatus? GetServiceControllerStatus()
        {
            var status = ServiceController
                .GetServices()
                .FirstOrDefault(x => x.ServiceName == "Mentoring2017Taturevich")
                ?.Status;

            return status;
        }

        public string GetLogInfo()
        {
            if (FileHelper.TryOpen(_serviceLogsPath, 5))
            {
                return File.ReadAllText(_serviceLogsPath);
            }

            return string.Empty;
        }

        public void ClearLogs()
        {
            if (File.Exists(_serviceLogsPath))
            {
                FileHelper.OpenFileWithoutSharing(_serviceLogsPath);
                File.Delete(_serviceLogsPath);
            }
        }

        private async Task ExecuteServiceCommandAsync(string commmandArgument)
        {
            _process.StartInfo.Arguments = commmandArgument;
            _process.Start();
            // Give service time to perform operation 
            await Task.Delay(1500);
        }
    }
}
