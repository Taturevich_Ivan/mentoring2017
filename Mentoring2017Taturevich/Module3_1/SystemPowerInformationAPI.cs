﻿using System;
using System.Runtime.InteropServices;
using Module3_1.InfoClasses;

namespace Module3_1
{
    /// <summary>
    /// First layer wrapper on powrprof.dll functions
    /// </summary>
    public static class SystemPowerInformationApi
    {
        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            InformationLevel informationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            out SYSTEM_BATTERY_STATE sbt,
            int nOutputBufferSize
        );

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            InformationLevel informationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            out SYSTEM_POWER_INFORMATION spi,
            int nOutputBufferSize
        );

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            InformationLevel informationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            out ulong spi,
            int nOutputBufferSize
        );

        [DllImport("powrprof.dll", SetLastError = true)]
        public static extern uint CallNtPowerInformation(
            InformationLevel informationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            IntPtr outputBuffer,
            int nOutputBufferSize
        );

        [DllImport("powrprof.dll", CharSet = CharSet.Auto, ExactSpelling = true, SetLastError = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        public struct SYSTEM_POWER_INFORMATION
        {
            public uint MaxIdlenessAllowed;
            public uint Idleness;
            public uint TimeRemaining;
            public byte CoolingMode;
        }

        public struct SYSTEM_BATTERY_STATE
        {
            [MarshalAs(UnmanagedType.I1)]
            public bool AcOnLine;
            [MarshalAs(UnmanagedType.I1)]
            public bool BatteryPresent;
            [MarshalAs(UnmanagedType.I1)]
            public bool Charging;
            [MarshalAs(UnmanagedType.I1)]
            public bool Discharging;
            public byte Spare0;
            public byte Spare1;
            public byte Spare2;
            public byte Spare3;
            public uint MaxCapacity;
            public uint RemainingCapacity;
            public int Rate;
            public uint EstimatedTime;
            public uint DefaultAlert1;
            public uint DefaultAlert2;
        }
    }
}
