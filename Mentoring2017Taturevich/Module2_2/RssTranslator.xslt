﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    exclude-result-prefixes="msxsl"
    xmlns:bn="http://library.by/catalog"
    extension-element-prefixes="bn">

  <xsl:strip-space elements="*"/>
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/bn:catalog">
    <rss version="2.0">
      <channel>
        <xsl:apply-templates />
      </channel>
    </rss>
  </xsl:template>

  <xsl:template match="bn:book">
    <item>
      <xsl:apply-templates />
    </item>
  </xsl:template>

  <xsl:template match="bn:title">
    <title>
      <xsl:apply-templates/>
    </title>
  </xsl:template>

  <xsl:template match="bn:description">
    <description>
      <xsl:apply-templates/>
    </description>
  </xsl:template>

  <xsl:template match="bn:registration_date">
    <pubDate>
      <xsl:apply-templates/>
    </pubDate>
  </xsl:template>

  <xsl:template match="bn:genre">
    <xsl:choose>
      <xsl:when test=".='Computer'">
        <xsl:if test="../bn:isbn">
          <link>
            http://my.safaribooksonline.com/<xsl:value-of select="../bn:isbn"/>
          </link>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="bn:author|bn:publish_date|bn:publisher|bn:isbn"/>

</xsl:stylesheet>
