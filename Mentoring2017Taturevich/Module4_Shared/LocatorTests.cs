﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Module4_Shared.Workers;

namespace Module4_Shared
{
    [TestClass]
    public class LocatorTests
    {
        [TestMethod]
        public void DomainElementsTest()
        {
            using (var locator = new IanaPageObject())
            {
                var list = locator.GetDomainsElementList();
                Assert.IsTrue(list.Count > 0);
            }
        }

        [TestMethod]
        public void DomainNamesTest()
        {
            using (var locator = new IanaPageObject())
            {
                var resultList = new List<string>();
                var list = locator.GetDomainsElementList();
                Assert.IsTrue(list.Count > 0);
                resultList.AddRange(locator.GetDomainsNameListFromElements(list));
                Assert.AreEqual(list.Count, resultList.Count);
            }
        }

        [TestMethod]
        public void WhoisTest()
        {
            var html = string.Empty;
            var url = "https://www.iana.org/whois?q=.aaa";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
                if (stream != null)
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        html = reader.ReadToEnd();
                    }
                }

            var startTag = @"<p id=""results"">" + "\n<pre>";
            var startIndex = html.IndexOf(startTag, StringComparison.Ordinal) + startTag.Length;
            var endIndex = html.IndexOf("</pre>", startIndex, StringComparison.Ordinal);
            var text = html.Substring(startIndex, endIndex - startIndex);

            Assert.AreEqual(text,
                "% IANA WHOIS server\n% for more information on IANA, visit http://www.iana.org\n% This query returned 1 object\n\ndomain: " +
                "      AAA\n\norganisation: American Automobile Association, Inc.\naddress:      1000 AAA Drive\naddress:      " +
                "Heathrow, FL 32746\naddress:      United States\n\ncontact:      administrative\nname:         " +
                "Chris Niemi\norganisation: MarkMonitor Inc.\naddress:      391 N. Ancestor Place\naddress:      Boise, ID 83704\naddress:      " +
                "United States\nphone:        (208) 389-5740\nfax-no:       (208) 389-5771\ne-mail:       gtldservices@markmonitor.com\n\ncontact:      " +
                "technical\nname:         Sean Kaine\norganisation: Neustar, Inc.\naddress:      46000 Center Oak Plaza\naddress:      " +
                "Sterling, VA  20166\naddress:      United States\nphone:        +15714345168\nfax-no:       +15714345401\ne-mail:       " +
                "registrytechnical1@neustar.biz\n\nnserver:      NS1.DNS.NIC.AAA 156.154.144.2 2610:a1:1071:0:0:0:0:2\nnserver:      " +
                "NS2.DNS.NIC.AAA 156.154.145.2 2610:a1:1072:0:0:0:0:2\nnserver:      NS3.DNS.NIC.AAA 156.154.159.2 2610:a1:1073:0:0:0:0:2\nnserver:      " +
                "NS4.DNS.NIC.AAA 156.154.156.2 2610:a1:1074:0:0:0:0:2\nnserver:      NS5.DNS.NIC.AAA 156.154.157.2 2610:a1:1075:0:0:0:0:2\nnserver:      " +
                "NS6.DNS.NIC.AAA 156.154.158.2 2610:a1:1076:0:0:0:0:2\nds-rdata:     21707 8 1 6D92DD0D0DB0E392FA9D5F08EA15BBD297B8CBE2\nds-rdata:     " +
                "21707 8 2 4F74856F31B73F3BFCDF430985329F55AA655BC9E53C4BF9DC6B14CCE6780600\nds-rdata:     " +
                "28192 8 1 563200F63B8B1797B4D88D14BD6A672EA4D0CC0C\nds-rdata:     " +
                "28192 8 2 DCB5AA6EC2B73D3E8C82D481770151160B38BCF2DBF3B9CD587AAD388D3572D7\n\nstatus:       " +
                "ACTIVE\nremarks:      Registration information: http://www.aaa.com\n\ncreated:      2015-08-13\nchanged:      2016-11-08\nsource:       IANA\n\n");
        }

        /// <summary>
        /// Use this method to fully clean up useless browser process.
        /// For example in case of breaking Debug process before disposal in the middle of IanaPageLocator routine
        /// </summary>
        [TestMethod]
        public void KillPhantomJsBrowsers()
        {
            IanaPageObject.BrutalKillAllPhantomBrowsers();
        }
    }
}
