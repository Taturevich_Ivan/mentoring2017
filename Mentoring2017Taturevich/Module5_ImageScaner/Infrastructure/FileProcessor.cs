﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Module5_ImageScaner.Infrastructure.Helpers;
using Module5_ImageScaner.Infrastructure.ReadModels;

namespace Module5_ImageScaner.Infrastructure
{
    public interface IFileProcessor
    {
        string BarcodeText { get; set; }

        OutputType OutputType { get; set; }

        string OutputDirectory { get; set; }

        string TemporaryDirectory { get; set; }

        void ProcessFileByName(string fileName);

        void StartProcessingFiles(List<string> fileNames);

        void StartReProcessing();
    }

    public class FileProcessor : IFileProcessor
    {
        private const string Dot = ".";
        private int _lastAddedFileNameNumber = default(int);
        private const string ImageStartNamePattern = "image_";
        private readonly PdfHelper _pdfHelper = ImageScannerKernelInstance.Get<PdfHelper>();
        private readonly BitmapHelper _bitmapHelper = ImageScannerKernelInstance.Get<BitmapHelper>();
        private readonly BarcodeManager _barcodeManager = ImageScannerKernelInstance.Get<BarcodeManager>();
        private readonly ConcurrentDictionary<string, string> _currentFileNamesSequence
            = new ConcurrentDictionary<string, string>();
        private readonly FileSenderMessageService _senderMessageService = new FileSenderMessageService();

        public string BarcodeText { get; set; }

        public OutputType OutputType { get; set; }

        public string OutputDirectory { get; set; }

        public string TemporaryDirectory { get; set; }

        public void StartProcessingFiles(List<string> fileNames)
        {
            fileNames.ForEach(ProcessFileByName);
            StartReProcessing();
        }

        public void ProcessFileByName(string fileName)
        {
            if (_currentFileNamesSequence.Values.Contains(fileName))
            {
                return;
            }

            if (IsFileHasBarCode(fileName))
            {
                if (_currentFileNamesSequence.Any())
                {
                    StartReProcessing();
                }
                FileHelper.TryDelete(fileName);
            }
            else if (File.Exists(fileName))
            {
                ParseFileByName(fileName);
            }
        }

        private bool IsFileHasBarCode(string fileName)
        {
            FileHelper.OpenFileWithoutSharing(fileName);
            return _barcodeManager.TryDetectBarcodeFromImage(fileName, BarcodeText);
        }

        private void ParseFileByName(string fileName)
        {
            var stringNumber = GetNumberPartFromFileName(fileName);
            if (!string.IsNullOrEmpty(stringNumber))
            {
                int parsedNumber;
                if (int.TryParse(stringNumber, out parsedNumber))
                {
                    HandleNewAddedFile(parsedNumber.ToString(), fileName);
                }
                else
                {
                    RelocateFile(fileName);
                }
            }
            else
            {
                RelocateFile(fileName);
            }
        }

        private void HandleNewAddedFile(string parsedFileNumber, string fileName)
        {
            if (!_currentFileNamesSequence.Any())
            {
                _currentFileNamesSequence.TryAdd(parsedFileNumber, fileName);
            }
            else
            {
                var compareResult = int.Parse(parsedFileNumber) - _lastAddedFileNameNumber;
                // Ascending and descending
                if (compareResult == 1 || compareResult == -1)
                {
                    _currentFileNamesSequence.TryAdd(parsedFileNumber, fileName);
                }
                else if (compareResult > 1 || compareResult == 0 && compareResult < 0)
                {
                    StartReProcessing();
                    _currentFileNamesSequence.TryAdd(parsedFileNumber, fileName);
                }
            }
            _lastAddedFileNameNumber = int.Parse(parsedFileNumber);
        }

        private string GetNumberPartFromFileName(string fileName)
        {
            var imageToken = $"{ImageStartNamePattern}";
            var startIndex = fileName.IndexOf(imageToken, StringComparison.Ordinal) + imageToken.Length;
            var endIndex = fileName.IndexOf(Dot, startIndex, StringComparison.Ordinal);
            var stringNumber = fileName.Substring(startIndex, endIndex - startIndex);

            return stringNumber;
        }

        public void StartReProcessing()
        {
            var fileNames = _currentFileNamesSequence.Values.Reverse().ToList();
            try
            {
                switch (OutputType)
                {
                    case OutputType.Pdf:
                        var pdfFileArray = _pdfHelper
                            .CreatePdfBytesArray(fileNames);
                        _senderMessageService.SendFileMessage(pdfFileArray, OutputDirectory, OutputType.Pdf);
                        break;
                    case OutputType.Tiff:
                        var tiffFileArray = _bitmapHelper
                            .CreateByteTiffImageFromUsedFileNames(fileNames);
                        _senderMessageService.SendFileMessage(tiffFileArray, OutputDirectory, OutputType.Tiff);
                        break;
                }
                FileHelper.TryDeleteMany(_currentFileNamesSequence.Values.ToList());
                _currentFileNamesSequence.Clear();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                fileNames.ForEach(RelocateFile);
            }
        }

        private void RelocateFile(string fileName)
        {
            var newPath = Path.Combine(TemporaryDirectory, Path.GetFileName(fileName));
            FileHelper.MoveFilewithOverWrite(fileName, newPath);
        }
    }
}
