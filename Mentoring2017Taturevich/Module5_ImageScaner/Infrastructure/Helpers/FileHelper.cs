﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Module5_ImageScaner.Infrastructure.Helpers
{
    public static class FileHelper
    {
        public const string GlobalSettingsFileName = "imageScannerSettings.xml";

        public static void MoveFilewithOverWrite(string source, string destination)
        {
            if (TryOpen(source, 3))
            {
                if (File.Exists(destination))
                {
                    File.Delete(destination);
                }

                File.Move(source, destination);
            }
        }

        public static bool TryOpen(string fileName, int tryCount)
        {
            for (int i = 0; i < tryCount; i++)
            {
                try
                {
                    OpenFileWithoutSharing(fileName);

                    return true;
                }
                catch (IOException)
                {
                    Thread.Sleep(500);
                }
            }

            return false;
        }

        public static void TryDeleteMany(IEnumerable<string> fileNames)
        {
            try
            {
                foreach (var fileName in fileNames)
                {
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        public static void TryDelete(string fileName)
        {
            try
            {
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
        }

        public static void OpenFileWithoutSharing(string fileName)
        {
            try
            {
                var file = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                file.Close();
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
