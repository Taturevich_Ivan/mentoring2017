﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Module1_2
{
    public class Mapper<TSource, TDestination>
    {
        private readonly Func<TSource, TDestination> _mapFunction;

        internal Mapper(Func<TSource, TDestination> func)
        {
            _mapFunction = func;
        }
        public TDestination Map(TSource source)
        {
            return _mapFunction(source);
        }
    }

    public static class MapperGenerator
    {
        public static Mapper<TSource, TDestination> CreateMapper<TSource, TDestination>()
        {
            return CreateMapperFunction<TSource, TDestination>();
        }

        public static Mapper<TSource, TDestination> CreateStructToClassMapper<TSource, TDestination>()
            where TSource : struct
            where TDestination : class
        {
            return CreateStructMapperFunction<TSource, TDestination>();
        }

        /// <summary>
        /// Simple mapping function member by member
        /// </summary>
        /// <returns></returns>
        private static Mapper<TSource, TDestination> CreateMapperFunction<TSource, TDestination>()
        {
            var source = Expression.Parameter(typeof(TSource));
            var body = Expression.MemberInit(Expression.New(typeof(TDestination)),
                source
                .Type
                .GetProperties()
                .Select(p => Expression
                    .Bind(typeof(TDestination).GetProperty(p.Name), Expression.Property(source, p))));
            var expression = Expression.Lambda<Func<TSource, TDestination>>(body, source);

            return new Mapper<TSource, TDestination>(expression.Compile());
        }

        /// <summary>
        /// Mapping fields from structure to properties of class
        /// </summary>
        /// <typeparam name="TSource">source structure</typeparam>
        /// <typeparam name="TDestination">destination class</typeparam>
        /// <returns></returns>
        private static Mapper<TSource, TDestination> CreateStructMapperFunction<TSource, TDestination>()
            where TSource : struct
            where TDestination : class
        {
            var source = Expression.Parameter(typeof(TSource));
            var body = Expression.MemberInit(Expression.New(typeof(TDestination)),
                source
                    .Type
                    .GetFields()
                    .Select(p => Expression
                        .Bind(typeof(TDestination).GetProperty(p.Name), Expression.Field(source, p))));
            var expression = Expression.Lambda<Func<TSource, TDestination>>(body, source);

            return new Mapper<TSource, TDestination>(expression.Compile());
        }
    }
}
