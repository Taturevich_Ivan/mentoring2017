﻿using System;

namespace Module5_ImageScaner.Infrastructure.ReadModels
{
    [Serializable]
    public enum FileMessageWatcherStatus
    {
        Stopped = 0,

        WaitingForFiles = 1,

        ProcessingFiles = 2
    }
}
