﻿namespace Module5_ImageScaner.Infrastructure.ReadModels
{
    public class HeadersModel
    {
        public string FileId { get; set; }

        public int ChunkCount { get; set; }

        public bool IsChunked { get; set; }

        public bool IsComplete { get; set; }

        public string SaveFilePath { get; set; }

        public OutputType OutputType { get; set; }
    }
}
