﻿using System;
using System.Collections.Generic;
using System.IO;
using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Shapes;
using MigraDoc.Rendering;

namespace Module5_ImageScaner.Infrastructure.Helpers
{
    public interface IPdfHelper
    {
        void CreatePdfDocumentFromUsedFileNames(List<string> fileNames, string outputDirectory);

        void SavePdfByByteArray(byte[] bytes, string outputDirectory);

        byte[] CreatePdfBytesArray(List<string> fileNames);
    }

    public class PdfHelper : IPdfHelper
    {
        public void CreatePdfDocumentFromUsedFileNames(List<string> fileNames, string outputDirectory)
        {
            var render = RenderDocument(fileNames);
            render.Save(GetFilePath(outputDirectory));
        }

        public void SavePdfByByteArray(byte[] bytes, string outputDirectory)
        {
            var fileName = GetFilePath(outputDirectory);
            using (var fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }
        }

        private string GetFilePath(string outputDirectory)
        {
            return Path.Combine(outputDirectory, $"{Guid.NewGuid()}.pdf");
        }

        public byte[] CreatePdfBytesArray(List<string> fileNames)
        {
            var render = RenderDocument(fileNames);

            using (MemoryStream stream = new MemoryStream())
            {
                render.Save(stream, true);
                return stream.ToArray();
            }
        }

        private PdfDocumentRenderer RenderDocument(List<string> fileNames)
        {
            var document = new Document();
            var section = document.AddSection();
            foreach (var fileName in fileNames)
            {
                var image = section.AddImage(fileName);
                image.Top = 0;
                image.Left = 0;
                image.Height = document.DefaultPageSetup.PageHeight;
                image.Width = document.DefaultPageSetup.PageWidth;
                image.RelativeHorizontal = RelativeHorizontal.Page;
                image.RelativeVertical = RelativeVertical.Page;
                section.AddPageBreak();
            }

            var render = new PdfDocumentRenderer { Document = document };
            render.RenderDocument();
            return render;
        }
    }
}
