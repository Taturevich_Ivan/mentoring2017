﻿namespace Sample03.E3SClient.Entities
{
    public static class QueryConstants
    {
        public static char Colon = ':';

        public static char StarSymbol = '*';

        public static char AndSeparator = '&';

        public static char LeftBracket = '(';

        public static char RightBracket = ')';

        public static string WhereStatememt = "Where";
    }
}
