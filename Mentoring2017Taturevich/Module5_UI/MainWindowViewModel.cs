﻿using AOPModule.Decorators;
using Module5_ImageScaner;
using Module5_ImageScaner.Infrastructure;
using Module5_ImageScaner.Infrastructure.Helpers;
using Module5_ImageScaner.Infrastructure.ReadModels;
using Module5_UI.Commands;
using Module5_UI.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Module5_UI
{
    [LoggingAspect]
    public class MainWindowViewModel : ViewModelBase
    {
        private int _delayTime;
        private string _serviceLog;
        private string _barcodeText;
        private string _outputFolder;
        private bool _isServiceExist;
        private OutputType _outputType;
        private string _temporaryFolder;
        private ICommand _selectCommand;
        private ICommand _removeCommand;
        private int _selectedfolderIndex;
        private ICommand _clearLogsCommand;
        private ICommand _runServiceCommand;
        private ICommand _stopServiceCommand;
        private ICommand _saveSettingsCommand;
        private ICommand _deleteServiceCommand;
        private ICommand _installServiceCommand;
        private ICommand _generateBarcodeCommand;
        private ICommand _selectOutputFolderCommand;
        private ICommand _selectTemporaryFolderCommand;
        private readonly IServiceManager _serviceManager;
        private readonly IBarcodeManager _barcodeManager;
        private ImageScannerOptions _imageScannerOptions;
        private ObservableCollection<string> _observedFolders;
        private ServiceControllerStatus _serviceControllerStatus;
        private readonly IFileSenderMessageService _fileSenderMessageService;

        public MainWindowViewModel()
        {
            _barcodeManager = ImageScannerKernelInstance.Get<IBarcodeManager>();
            _imageScannerOptions = ImageScannerSerializer
                .DeserializeObject();
            MatchScannerOptions(_imageScannerOptions);
            _serviceManager = UIKernelInstance.Get<IServiceManager>();
            ServiceControllerStatus = ServiceControllerStatus.Stopped;
            NotifyOnServiceChanges();
            _fileSenderMessageService = UIKernelInstance.Get<IFileSenderMessageService>();
        }

        private void MatchScannerOptions(ImageScannerOptions imageScannerOptions)
        {
            DelayTime = imageScannerOptions.DelayTime;
            OutputType = imageScannerOptions.OutputType;
            BarcodeText = imageScannerOptions.BarcodeText ?? string.Empty;
            OutputFolder = imageScannerOptions.OutputFolder ?? string.Empty;
            ObservedFolders = new ObservableCollection<string>(
                imageScannerOptions.ObservedFolders);
            TemporaryFolder = imageScannerOptions.TemporaryFolder ?? string.Empty;
        }

        public List<OutputType> OutputTypes { get; }
            = Enum.GetValues(typeof(OutputType)).Cast<OutputType>().ToList();

        public int DelayTime
        {
            get { return _delayTime; }
            set { SetField(ref _delayTime, value); }
        }

        public string BarcodeText
        {
            get { return _barcodeText; }
            set { SetField(ref _barcodeText, value); }
        }

        public ServiceControllerStatus ServiceControllerStatus
        {
            get { return _serviceControllerStatus; }
            set { SetField(ref _serviceControllerStatus, value); }
        }

        public bool IsServiceExist
        {
            get { return _isServiceExist; }
            set { SetField(ref _isServiceExist, value); }
        }

        public OutputType OutputType
        {
            get { return _outputType; }
            set { SetField(ref _outputType, value); }
        }

        public string TemporaryFolder
        {
            get { return _temporaryFolder; }
            set { SetField(ref _temporaryFolder, value); }
        }

        public string OutputFolder
        {
            get { return _outputFolder; }
            set { SetField(ref _outputFolder, value); }
        }

        public ObservableCollection<string> ObservedFolders
        {
            get { return _observedFolders; }
            set { SetField(ref _observedFolders, value); }
        }

        public ImageScannerOptions ImageScannerOptions
        {
            get { return _imageScannerOptions; }
            set { SetField(ref _imageScannerOptions, value); }
        }

        public ICommand SaveSettingsCommand
        {
            get
            {
                return _saveSettingsCommand ?? (_saveSettingsCommand = new RelayCommand(
                    p => IsSelectedSettingsValid(), async p =>
                    {
                        await RunAsync(async () =>
                        {
                            if (ServiceControllerStatus == ServiceControllerStatus.Running)
                            {
                                RunInAppartmentThread(() => MessageBox.Show(Resources
                                    .MainWindowViewModel_SaveSettingsCommand_It_will_stops_service_from_running_));
                                await _serviceManager.Stop();
                                ApplyChanges();
                                SaveChangesInGlobalSettings();
                            }
                            else
                            {
                                ApplyChanges();
                                SaveChangesInGlobalSettings();
                            }
                            NotifyOnServiceChanges();
                        });
                        RunInAppartmentThread(() => MessageBox.Show(
                            Resources.MainWindowViewModel_SaveSettingsCommand_Settings_was_successfully_saved));
                    }));
            }
        }

        private bool IsSelectedSettingsValid()
        {
            var isDefaultSettingsValid = ObservedFolders.Any()
                && !string.IsNullOrEmpty(OutputFolder)
                && !string.IsNullOrEmpty(TemporaryFolder);

            var isSeconadarySettingsValid = !string.IsNullOrEmpty(BarcodeText);

            return isDefaultSettingsValid && isSeconadarySettingsValid;
        }

        public ICommand SelectOutputFolderCommand
        {
            get
            {
                return _selectOutputFolderCommand ?? (_selectOutputFolderCommand = new RelayCommand(
                    p => true, async p => await SelectOutputFolderDialog()));
            }
        }

        private async Task SelectOutputFolderDialog()
        {
            var selectedPath = await CallSelectionFolderDialog(Resources
                .MainWindowViewModel_SelectOutputFolderDialog_Select_folder_for_output_files);
            OutputFolder = selectedPath;
        }

        public ICommand SelectInputFolderCommand
        {
            get
            {
                return _selectCommand ?? (_selectCommand = new RelayCommand(
                    p => true, async p => await SelectInputFolderDialog()));
            }
        }

        public object RemoveInputFolderCommand
        {
            get
            {
                return _removeCommand ?? (_removeCommand = new RelayCommand(
                    p => ObservedFolders.Any()
                        && SelectedFolderIndex >= 0
                        && SelectedFolderIndex < ObservedFolders.Count
                        && ObservedFolders[SelectedFolderIndex] != null,
                    p => ObservedFolders.RemoveAt(SelectedFolderIndex)));
            }
        }

        public int SelectedFolderIndex
        {
            get { return _selectedfolderIndex; }
            set
            {
                if (value >= 0)
                {
                    SetField(ref _selectedfolderIndex, value);
                }
            }
        }

        public object SelectTemporaryFolderCommand
        {
            get
            {
                return _selectTemporaryFolderCommand ?? (_selectTemporaryFolderCommand = new RelayCommand(
                    p => true, async p => await SelectTemporaryFolderDialog()));
            }
        }

        public object RunServiceCommand
        {
            get
            {
                return _runServiceCommand ?? (_runServiceCommand = new RelayCommand(
                    p => ServiceControllerStatus != ServiceControllerStatus.Running && IsServiceExist,
                    async p => await RunAsync(async () =>
                    {
                        await _serviceManager.Run();
                        NotifyOnServiceChanges();
                    })));
            }
        }

        public object StopServiceCommand
        {
            get
            {
                return _stopServiceCommand ?? (_stopServiceCommand = new RelayCommand(
                    p => ServiceControllerStatus != ServiceControllerStatus.Stopped && IsServiceExist,
                    async p => await RunAsync(async () =>
                   {
                       await _serviceManager.Stop();
                       NotifyOnServiceChanges();
                   })));
            }
        }

        public object DeleteServiceCommand
        {
            get
            {
                return _deleteServiceCommand ?? (_deleteServiceCommand = new RelayCommand(
                    p => IsServiceExist, async p => await RunAsync(async () =>
                    {
                        await _serviceManager.UnInstall();
                        NotifyOnServiceChanges();
                    })));
            }
        }

        public string ServiceLog
        {
            get { return _serviceLog; }
            set { SetField(ref _serviceLog, value); }
        }

        public ICommand InstallServiceCommand
        {
            get
            {
                return _installServiceCommand ?? (_installServiceCommand = new RelayCommand(
                    p => !IsServiceExist,
                    async p => await RunAsync(async () =>
                    {
                        await _serviceManager.Install();
                        NotifyOnServiceChanges();
                    })));
            }
        }

        public ICommand ClearLogsCommand
        {
            get
            {
                return _clearLogsCommand ?? (_clearLogsCommand = new RelayCommand(
                    p => true, async p => await RunAsync(() =>
                    {
                        _serviceManager.ClearLogs();
                        NotifyOnServiceChanges();
                    })));
            }
        }

        public ICommand GenerateBarcodeCommand
        {
            get
            {
                return _generateBarcodeCommand ?? (_generateBarcodeCommand = new RelayCommand(
                    p => !string.IsNullOrEmpty(BarcodeText),
                    async p => await RunAsync(() =>
                    {
                        _barcodeManager.CreateBarCodeFromText(BarcodeText);
                    })));
            }
        }

        private async Task SelectTemporaryFolderDialog()
        {
            var selectedPath = await CallSelectionFolderDialog(
                Resources.MainWindowViewModel_CallSelectionFolderDialog_Select_temporary_folder);
            TemporaryFolder = selectedPath;
        }

        private async Task SelectInputFolderDialog()
        {
            var selectedPath = await CallSelectionFolderDialog(Resources
                .MainWindow_OpenFileButton_Click_Select_folder_with_images_to_process);
            ObservedFolders.Add(selectedPath);
        }

        private void ApplyChanges()
        {
            ImageScannerOptions.DelayTime = DelayTime;
            ImageScannerOptions.OutputType = OutputType;
            ImageScannerOptions.BarcodeText = BarcodeText;
            ImageScannerOptions.OutputFolder = OutputFolder;
            ImageScannerOptions.TemporaryFolder = TemporaryFolder;
            ImageScannerOptions.ObservedFolders = ObservedFolders.ToList();
        }

        private void SaveChangesInGlobalSettings()
        {
            ImageScannerSerializer.SerializeObjectAndSaveInFile(ImageScannerOptions);
            if (ServiceControllerStatus == ServiceControllerStatus.Running)
            {
                _fileSenderMessageService.SendTypeMessage("Settings was changed", "ScannerSettingsChanged");
            }
        }

        private async Task<string> CallSelectionFolderDialog(string description)
        {
            string selectedPath = null;
            await RunAsync(() =>
            {
                RunInAppartmentThread(() =>
                {
                    var dialog = new FolderBrowserDialog
                    {
                        Description = description
                    };
                    selectedPath = dialog.ShowDialog()
                        == DialogResult.OK
                        ? dialog.SelectedPath : string.Empty;
                    ImageScannerOptions.ObservedFolders.Add(selectedPath);
                });
            });

            return selectedPath;
        }

        private void NotifyOnServiceChanges()
        {
            ServiceLog = _serviceManager.GetLogInfo();
            IsServiceExist = _serviceManager.IsServiceExist;
            ServiceControllerStatus = _serviceManager
                .GetServiceControllerStatus() ?? ServiceControllerStatus.Stopped;
        }
    }
}
