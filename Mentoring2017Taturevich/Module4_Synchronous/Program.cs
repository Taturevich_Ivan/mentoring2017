﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Module4_Shared.Workers;

namespace Module4_Synchronous
{
    static class Program
    {
        private static int _progressCount;
        private static WhoisParser Parser { get; } = new WhoisParser();
        private static DomainFileHelper FileHelper { get; } = new DomainFileHelper();
        private static PerformanceCounter CpuCounter { get; } = new PerformanceCounter("Processor", "% Processor Time", "_Total");

        private static List<float> CpuCountList { get; } = new List<float>();

        static void Main()
        {
            var watcher = new Stopwatch();
            watcher.Start();
            using (var locator = new IanaPageObject())
            {
                Console.Clear();
                Console.WriteLine("Synchronous test started...");
                Console.CursorVisible = false;
                var domains = locator.GetDomainsElementList();
                foreach (var domainName in locator.GetDomainsNameListFromElements(domains))
                {
                    ProcessDomainFile(domainName);
                }
            }

            watcher.Stop();
            Console.WriteLine();
            Console.WriteLine($"\nSpent time: {watcher.Elapsed}");
            Console.WriteLine($"Average CPU usage: {CpuCountList.Average()}%");
            Console.CursorVisible = true;
            Console.WriteLine($"Directory with result files: \n{FileHelper.BaseDataFileFolder}");
            Console.ReadLine();
        }

        private static void ProcessDomainFile(object domainName)
        {
            var convertedName = domainName.ToString();
            var domainInfo = Parser.GetInformationByDomainName(convertedName);
            SafeToFile(new object[] { domainInfo, convertedName });
        }

        private static void SafeToFile(object info)
        {
            var array = info as object[];
            if (array != null)
            {
                var text = array[0].ToString();
                var name = array[1].ToString();
                FileHelper.WriteDomainInfoFile(text, name);
                Interlocked.Increment(ref _progressCount);
                CpuCountList.Add(CpuCounter.NextValue());
                Console.Write($"Files created: {_progressCount}/{DomainFileHelper.ResultFileCount}." +
                              $" Threads in use: {Process.GetCurrentProcess().Threads.Count}\r");
            }
        }
    }
}
