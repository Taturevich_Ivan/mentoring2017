This is EPAM mentoring 2017 repository.

Instructions to install RabbitMQ server to test module 6 task.

- install and run Erlang Windows Binary File http://www.erlang.org/downloads (OTP 19.3 Windows 64-bit Binary File (103012097)) 
- download and run rabbitmq-server-3.6.9.exe https://www.rabbitmq.com/install-windows.html
- open RabbitMQ command promt and run following command: rabbitmq-plugins enable rabbitmq_management
- open in browser http://localhost:15672. Type user: guest password: guest
- if authorization was successfull, open project run Module5_UI and Module6_MainServer (by default settings set to start this projects together)
- if no errors during at this stage you should see something like this: https://snag.gy/UImPw5.jpg. Mentoring2017FileQueue - queue for file transfer and
Mentoring2017SettingsQueue for settings transfer
Now environment is ready for testing!

Instructions for module 7 (Debugging) set up.

- build project Module7_Debugging
- move all files from Module7_Debugging/Resources folder to Module7_Debugging/bin/Debug
- set break point to 25 line: var test = new CrackMe.Form1();
- run programm. On break point make step into (F11)
- you should occur in IL code 
- set break point to 822 line: IL_0145:  call       !!0[] [System.Core]System.Linq.Enumerable::ToArray<int32>(class [mscorlib]System.Collections.Generic.IEnumerable`1<!!0>)
- click continue, type something in form and you get the step with key values (see PasswordPlace.docx Result view section on screen)
- you should write this values with this separator: "-"  For example key for 5/19/2017: 100-80-2510-2160-480-1480

Instructions for module 10 (AOP) testing.
To test Task10 about AOP you need to run the application from a task about Windows services. Module5_UI. 
Log files are written in two directories: Module5_UI / bin / Debug and to the root of the project.