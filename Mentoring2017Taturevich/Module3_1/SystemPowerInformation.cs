﻿using Module1_2;
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Module3_1.InfoClasses;

namespace Module3_1
{
    /// <summary>
    /// Library which is provide information about system power
    /// </summary>
    public class SystemPowerInformation
    {
        public PowerInfo GetPowerInformation()
        {
            PowerInfo result;
            var mapper = MapperGenerator
                .CreateStructToClassMapper<SystemPowerInformationApi.SYSTEM_POWER_INFORMATION, PowerInfo>();

            SystemPowerInformationApi.SYSTEM_POWER_INFORMATION spi;
            uint retval = SystemPowerInformationApi.CallNtPowerInformation(
                InformationLevel.SystemPowerInformation,
                IntPtr.Zero,
                0,
                out spi,
                Marshal.SizeOf(typeof(SystemPowerInformationApi.SYSTEM_POWER_INFORMATION))
            );
            if (retval == 0)
            {
                result = mapper.Map(spi);
            }
            else
            {
                throw new Win32Exception((int)retval);
            }

            return result;
        }

        public BatteryState GetBatteryState()
        {
            BatteryState result;
            var mapper = MapperGenerator
                .CreateStructToClassMapper<SystemPowerInformationApi.SYSTEM_BATTERY_STATE, BatteryState>();

            SystemPowerInformationApi.SYSTEM_BATTERY_STATE sbt;
            uint retval = SystemPowerInformationApi.CallNtPowerInformation(
                InformationLevel.SystemBatteryState,
                IntPtr.Zero,
                0,
                out sbt,
                Marshal.SizeOf(typeof(SystemPowerInformationApi.SYSTEM_BATTERY_STATE))
            );
            if (retval == 0)
            {
                result = mapper.Map(sbt);
            }
            else
            {
                throw new Win32Exception((int)retval);
            }

            return result;
        }

        public TimeSpan GetLastSleepTime()
        {
            TimeSpan resultTime;
            ulong sleepTime;
            uint retval = SystemPowerInformationApi.CallNtPowerInformation(
                InformationLevel.LastSleepTime,
                IntPtr.Zero,
                0,
                out sleepTime,
                Marshal.SizeOf(typeof(ulong))
            );
            if (retval == 0)
            {
                var timeInSeconds = sleepTime * 0.000000001;
                resultTime = DateTime.Now.AddSeconds(timeInSeconds) - DateTime.Now;
            }
            else
            {
                throw new Win32Exception((int)retval);
            }

            return resultTime;
        }

        public TimeSpan GetLastWakeTime()
        {
            TimeSpan resultTime;
            ulong wakeTime;
            uint retval = SystemPowerInformationApi.CallNtPowerInformation(
                InformationLevel.LastWakeTime,
                IntPtr.Zero,
                0,
                out wakeTime,
                Marshal.SizeOf(typeof(ulong))
            );
            if (retval == 0)
            {
                var timeInSeconds = wakeTime * 0.000000001;
                resultTime = DateTime.Now.AddSeconds(timeInSeconds) - DateTime.Now;
            }
            else
            {
                throw new Win32Exception((int)retval);
            }

            return resultTime;
        }

        public void SetSuspendState(bool isHibernate)
        {
            SystemPowerInformationApi.SetSuspendState(isHibernate, false, false);
        }

        public void ReserveHibernationFile(bool isReserved)
        {
            int hiberParam = isReserved ? 1 : 0;
            var pointer = Marshal.AllocHGlobal(sizeof(int));
            Marshal.WriteInt32(pointer, hiberParam);
            uint retval = SystemPowerInformationApi.CallNtPowerInformation(
                InformationLevel.SystemReserveHiberFile,
                pointer,
                Marshal.SizeOf(sizeof(int)),
                IntPtr.Zero,
                0);
            Marshal.FreeHGlobal(pointer);
            if (retval != 0)
            {
                throw new Win32Exception((int)retval);
            }
        }
    }
}
