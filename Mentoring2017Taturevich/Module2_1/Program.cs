﻿using System;
using System.Xml;
using System.Xml.Schema;

namespace Module2_1
{
    static class Program
    {
        static void Main()
        {
            var settings = new XmlReaderSettings();

            settings.Schemas.Add("http://library.by/catalog", "../../booksSchema.xsd");
            settings.ValidationEventHandler += (sender, eventArgs)
                => Console.WriteLine("[Line {0}:Position {1}] {2}\n",
                    eventArgs.Exception.LineNumber,
                    eventArgs.Exception.LinePosition,
                    eventArgs.Message);
            settings.ValidationFlags = settings.ValidationFlags | XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationType = ValidationType.Schema;

            ReadXmlFile("../../books.xml", settings);
            ReadXmlFile("../../booksWithErrors.xml", settings);
            Console.ReadKey();
        }

        public static void ReadXmlFile(string filePath, XmlReaderSettings settings)
        {
            var reader = XmlReader.Create(filePath, settings);
            while (reader.Read())
            {
            }
        }
    }
}
