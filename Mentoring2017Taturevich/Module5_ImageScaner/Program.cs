﻿using System;
using System.Diagnostics;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;
using Topshelf;

namespace Module5_ImageScaner
{
    static class Program
    {
        static void Main(string[] args)
        {
            var currentDirectory = Path.GetDirectoryName(
                    Process.GetCurrentProcess().MainModule.FileName);
            var logConfig = new LoggingConfiguration();
            if (currentDirectory != null)
            {
                var target = new FileTarget()
                {
                    Name = "Def",
                    FileName = Path.Combine(currentDirectory, "log.txt"),
                    Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
                };

                logConfig.AddTarget(target);
                logConfig.AddRuleForAllLevels(target);
            }

            var logFactory = new LogFactory(logConfig);

            HostFactory.Run(
                conf =>
                {
                    conf.SetDisplayName("Mentoring2017Taturevich");
                    conf.SetServiceName("Mentoring2017Taturevich");
                    conf.SetDescription("Mentoring Taturevich Windows Service");
                    conf.Service<FileHandler>(
                            serv =>
                            {
                                serv.ConstructUsing(() => new FileHandler());
                                serv.WhenStarted(s => s.Start());
                                serv.WhenStopped(s => s.Stop());
                            }
                        ).UseNLog(logFactory);
                }
            );
            Console.Read();
        }
    }
}
