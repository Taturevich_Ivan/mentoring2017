﻿using System;
using System.Linq.Expressions;

namespace Module1_1.ExpressionVisitors
{
    public static class ExpressionExtension
    {
        public static Expression<Func<T>> ToLambdaExpression<T>(this Expression expression)
        {
            return Expression.Lambda<Func<T>>(expression);
        }
    }
}
