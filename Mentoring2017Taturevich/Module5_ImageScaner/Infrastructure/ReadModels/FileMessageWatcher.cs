﻿using System;
using System.IO;

namespace Module5_ImageScaner.Infrastructure.ReadModels
{
    public class FileMessageWatcher : IDisposable
    {
        public FileMessageWatcher(
            FileSystemWatcher fileSystemWatcher,
            FileSenderMessageService fileSenderMessageService)
        {
            FileSystemWatcher = fileSystemWatcher;
            FileSenderMessageService = fileSenderMessageService;
            WatcherId = Guid.NewGuid().ToString();
        }

        public string WatcherId { get; }

        public FileSystemWatcher FileSystemWatcher { get; }

        public ImageScannerOptions ImageScannerOptions { get; set; }

        public FileMessageWatcherStatus CurrentStatus { get; set; }

        public FileSenderMessageService FileSenderMessageService { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                FileSystemWatcher.Dispose();
                FileSenderMessageService.Dispose();
            }
        }
    }
}
