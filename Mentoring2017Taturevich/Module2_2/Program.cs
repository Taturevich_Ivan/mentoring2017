﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Xsl;

namespace Module2_2
{
    static class Program
    {
        static void Main()
        {
            var outputFilePath = "../../rssLine.rss";
            var xsl = new XslCompiledTransform();
            xsl.Load("../../RssTranslator.xslt");
            xsl.Transform("../../books.xml", outputFilePath);
            var finalOutput = GetOutputXmlString(outputFilePath);
            Console.WriteLine("Generated rss feed:\r\n");
            Console.WriteLine(finalOutput);

            Console.ReadKey();
        }

        /// <summary>
        /// Formatting function in case of bad indentation
        /// </summary>
        /// <param name="fileName">output XML file path</param>
        /// <returns>result XML as string</returns>
        static string GetOutputXmlString(string fileName)
        {
            var mStream = new MemoryStream();
            var xmlDocument = new XmlDocument();
            var writer = new XmlTextWriter(mStream, Encoding.Unicode)
            {
                Formatting = Formatting.Indented
            };
            try
            {
                xmlDocument.Load(fileName);
                xmlDocument.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();
                mStream.Position = 0;
                mStream.Position = 0;
                var streamReader = new StreamReader(mStream);
                var outputString = streamReader.ReadToEnd();

                return outputString;
            }
            catch (XmlException e)
            {
                Console.WriteLine(e);
                throw;
            }
            finally
            {
                mStream.Close();
                writer.Close();
            }
        }
    }
}
