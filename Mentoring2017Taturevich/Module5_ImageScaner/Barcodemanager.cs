﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using Module5_ImageScaner.Infrastructure.Helpers;
using ZXing;

namespace Module5_ImageScaner
{
    public interface IBarcodeManager
    {
        bool TryDetectBarcodeFromImage(string pathToFile, string barcodeText);

        void CreateBarCodeFromText(string textForEncoding);
    }

    public class BarcodeManager : IBarcodeManager
    {
        public string PathToFile { get; set; }

        public ImageFormat ImageFormat { get; set; }

        private readonly Regex _rxDigits = new Regex(@"[^\d]");

        public BarcodeManager()
        {
            ImageFormat = ImageFormat.Png;
            PathToFile = Path.GetFullPath(
                Path.Combine(Directory.GetCurrentDirectory(),
                    @"..\..\..\Module5_ImageScaner\QRCodes"));
        }

        public BarcodeManager(string pathToFile)
        {
            ImageFormat = ImageFormat.Png;
            PathToFile = pathToFile;
        }

        public bool TryDetectBarcodeFromImage(string pathToFile, string barcodeText)
        {
            var reader = new BarcodeReader();
            var file = File.OpenRead(pathToFile);
            var bitMap = new Bitmap(file);
            file.Close();
            var decodedResult = reader.Decode(bitMap);

            if (decodedResult == null)
            {
                return false;
            }

            if (decodedResult.Text == barcodeText)
            {
                return true;
            }

            FileHelper.TryDelete(pathToFile);
            return false;
        }

        public void CreateBarCodeFromText(string textForEncoding)
        {
            var writer = new BarcodeWriter
            {
                Format = BarcodeFormat.QR_CODE
            };
            var result = writer.Write(textForEncoding);
            var barcodeBitmap = new Bitmap(result);
            var generatedTime = DateTime.Now.ToString(CultureInfo.InvariantCulture);
            var codeName = _rxDigits.Replace(generatedTime, string.Empty);
            barcodeBitmap.Save($"{PathToFile}" +
                               $"/{codeName}" +
                               $".{ImageFormat}", ImageFormat);
        }
    }
}
