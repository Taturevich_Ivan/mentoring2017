﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
  version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:scr="urn:scripts"
  exclude-result-prefixes="msxsl"
  xmlns:bn="http://library.by/catalog"
  extension-element-prefixes="bn">

  <xsl:strip-space elements="*"/>
  <xsl:output method="html" indent="yes"/>

  <msxsl:script language="C#" implements-prefix="scr">
    <![CDATA[
      public string ExtractUrl(string str)
      {
          return Regex.Match(str, @"(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*))").Value;
      }
    ]]>
  </msxsl:script>

  <msxsl:script implements-prefix='scr' language='CSharp'>
    <![CDATA[public string CurrentDate() 
    {
        return System.DateTime.Now.ToShortDateString();
    }]]>
  </msxsl:script>

  <xsl:template match="/bn:catalog">
    <HTML>
      <HEAD>
        <H1>Current funds by genre</H1>
        <H2>
          Report Date: <xsl:value-of select="scr:CurrentDate()"/>
        </H2>
      </HEAD>
      <BODY>
        <xsl:call-template name="createGenreTable">
          <xsl:with-param name="genreName" select='"Computer"'/>
        </xsl:call-template>
        <xsl:call-template name="createGenreTable">
          <xsl:with-param name="genreName" select='"Fantasy"'/>
        </xsl:call-template>
        <xsl:call-template name="createGenreTable">
          <xsl:with-param name="genreName" select='"Romance"'/>
        </xsl:call-template>
        <xsl:call-template name="createGenreTable">
          <xsl:with-param name="genreName" select='"Horror"'/>
        </xsl:call-template>
        <xsl:call-template name="createGenreTable">
          <xsl:with-param name="genreName" select='"Science Fiction"'/>
        </xsl:call-template>
        <br/>
        <h2>
          Total number of books in library: <xsl:value-of select="count(//bn:book)"/>
        </h2>
      </BODY>
    </HTML>
  </xsl:template>

  <xsl:template name="createGenreTable">
    <xsl:param name="genreName"/>
    <h3>
      Books of <xsl:value-of select="$genreName"/>
    </h3>
    <table border="1" margin="0" padding="0">
      <xsl:call-template name="bookHeaderBuilder"/>
      <xsl:for-each select="//bn:genre[text()=$genreName]">
        <xsl:call-template name="bookRowBuilder">
          <xsl:with-param name="book" select="./bn:book"/>
        </xsl:call-template>
      </xsl:for-each>
    </table>
    <b>
      Total books of genre: <xsl:value-of select="count(//bn:genre[text()=$genreName])"/>
    </b>
  </xsl:template>

  <xsl:template name="bookRowBuilder">
    <xsl:param name="book"/>
    <tr>
      <td>
        <xsl:value-of select="../bn:author"/>
      </td>
      <td>
        <xsl:value-of select="../bn:title"/>
      </td>
      <td>
        <xsl:value-of select="../bn:publish_date"/>
      </td>
      <td>
        <xsl:value-of select="../bn:registration_date"/>
      </td>
    </tr>
  </xsl:template>

  <xsl:template name="bookHeaderBuilder">
    <tr>
      <th>
        <b>Author</b>
      </th>
      <th>
        <b>Title</b>
      </th>
      <th>
        <b>Publish Date</b>
      </th>
      <th>
        <b>Registration Date</b>
      </th>
    </tr>
  </xsl:template>


  <xsl:template match="text()|@*"/>

</xsl:stylesheet>
