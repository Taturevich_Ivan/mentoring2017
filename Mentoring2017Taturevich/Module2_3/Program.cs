﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Xsl;

namespace Module2_3
{
    static class Program
    {
        static void Main()
        {
            var outputFilePath = "../../libraryReport.html";

            var xsl = new XslCompiledTransform();
            var settings = new XsltSettings
            {
                EnableScript = true
            };
            xsl.Load("../../HtmlTranslator.xslt", settings, null);
            xsl.Transform("../../books.xml", outputFilePath);
            var path = Path.Combine(Directory.GetCurrentDirectory(), outputFilePath);

            Console.WriteLine("Go to your default browser to see output.");
            Process.Start(path);
        }
    }
}
