﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Module5_ImageScaner.Infrastructure.ReadModels;

namespace Module5_ImageScaner.Infrastructure
{
    public class WatcherManager<T>
    {
        private List<FileSystemWatcher> FileSystemWatchers
            => WorkerWatchers.Select(x => x.FileSystemWatcher).ToList();

        public List<string> ObservedDirectories { get; } = new List<string>();

        private readonly CancellationTokenSource _tokenSource = new CancellationTokenSource();

        public List<FileMessageWatcher> WorkerWatchers { get; } = new List<FileMessageWatcher>();

        private readonly ManualResetEventSlim _resetEvent = new ManualResetEventSlim(true);

        private readonly Action<string> _workerThreadAction;

        public WatcherManager(Action<string> workerAction)
        {
            _workerThreadAction = workerAction;
            Task.Factory.StartNew(() => SendStatusMessage(_tokenSource.Token));
        }

        private async Task SendStatusMessage(CancellationToken token)
        {
            while (true)
            {
                _resetEvent.Wait(token);
                WorkerWatchers
                    .ForEach(x => x.FileSenderMessageService
                        .SendMessageStatus(new FileWatcherStatusMessage(
                            x.WatcherId,
                            x.ImageScannerOptions,
                            x.CurrentStatus)));
                await Task.Delay(3000, token);
            }
        }

        public void StartProcessing()
        {
            ObservedDirectories
                .ForEach(obsd => Task
                    .Factory
                    .StartNew(
                        () => _workerThreadAction(obsd)));
            _resetEvent.Set();
        }

        public void EnableEventRaising()
        {
            FileSystemWatchers.ForEach(x => x.EnableRaisingEvents = true);
        }

        public void DisableEventRaising()
        {
            FileSystemWatchers.ForEach(x => x.EnableRaisingEvents = false);
            _resetEvent.Reset();
        }

        public void SetWatcherSettings(
            string observedFolderPath,
            FileMessageWatcherStatus status,
            ImageScannerOptions scanerOptions)
        {
            var workedWatcher = WorkerWatchers
                .FirstOrDefault(x => x.FileSystemWatcher.Path == observedFolderPath);
            if (workedWatcher != null)
            {
                workedWatcher.CurrentStatus = status;
                workedWatcher.ImageScannerOptions = scanerOptions;
            }
        }
    }
}
