﻿using Module5_ImageScaner;
using Module5_ImageScaner.Infrastructure;
using Ninject;
using Ninject.Extensions.Interception.Infrastructure.Language;

namespace Module5_UI
{
    public static class UIKernelInstance
    {
        private static readonly StandardKernel Kernel = new StandardKernel(
            new NinjectSettings { LoadExtensions = true });

        static UIKernelInstance()
        {
            Kernel.Load(new AopModule());
            Kernel.Bind<IFileSenderMessageService>().To<FileSenderMessageService>().Intercept().With<SerializationInterceptor>();
            Kernel.Bind<IBarcodeManager>().To<BarcodeManager>().Intercept().With<SerializationInterceptor>();
            Kernel.Bind<IServiceManager>().To<ServiceManager>().Intercept().With<SerializationInterceptor>();
        }

        public static MainWindowViewModel MainWindowViewModel => Kernel.Get<MainWindowViewModel>();

        public static T Get<T>() => Kernel.Get<T>();
    }
}
