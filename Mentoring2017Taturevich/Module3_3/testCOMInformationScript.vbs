set test = CreateObject("Module3_2.SystemPowerInformationComObject")
identity = test.GetIdentity()
powerInformation = test.GetPowerInformation()
batteryState = test.GetBatteryState()
lastSleepTime = test.GetLastSleepTime()
lastWakeTime = test.GetLastWakeTime()

resultString = "COM object was successfully registered with ID: " &identity
resultString = resultString &vbNewLine&vbNewLine& "SYSTEM POWER INFORMATION"
resultString = resultString &vbNewLine&powerInformation
resultString = resultString &vbNewLine&batteryState
resultString = resultString &vbNewLine&lastSleepTime
resultString = resultString &vbNewLine&lastWakeTime

WScript.Echo resultString
