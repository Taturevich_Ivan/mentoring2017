﻿using AOPModule.Proxy;
using Ninject.Modules;

namespace AOPModule
{
    public class AopModule : NinjectModule
    {
        public override void Load()
        {
            Bind<SerializationInterceptor>().ToSelf();
        }
    }
}
