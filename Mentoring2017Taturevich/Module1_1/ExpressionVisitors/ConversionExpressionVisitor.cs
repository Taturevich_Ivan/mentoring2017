﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace Module1_1.ExpressionVisitors
{
    /// <summary>
    /// Expression visitors for conversions:
    /// - basic binary operation to unary (if it possible)
    /// - lambda expression parameters to constants
    /// </summary>
    public class ConversionExpressionVisitor : ExpressionVisitor
    {
        private const int UnitValue = 1;
        private readonly Dictionary<string, object> _convertedParameters = new Dictionary<string, object>();

        public ConversionExpressionVisitor()
        {
        }

        public ConversionExpressionVisitor(
            Dictionary<string, object> convertedParameters)
        {
            _convertedParameters = convertedParameters;
        }

        /// <summary>
        /// Visit lambda expression and replace parameters with constants
        /// </summary>
        /// <param name="node">converted expression</param>
        /// <returns></returns>
        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            ////var ignoredParameters = node.Parameters.Where(parameter => !_convertedParameters.ContainsKey(parameter.Name));

            return Expression.Lambda(Visit(node.Body));
        }

        /// <summary>
        /// Check if parameter in "replace dictionary" and replace it with value
        /// </summary>
        /// <param name="node">visited node</param>
        /// <returns>handled node</returns>
        protected override Expression VisitParameter(ParameterExpression node)
        {
            var parameterName = node.Name;

            if (_convertedParameters.ContainsKey(parameterName))
            {
                Expression resultExpression = Expression.Constant(_convertedParameters[parameterName]);
                return resultExpression;
            }

            return base.VisitParameter(node);
        }

        protected override Expression VisitBinary(BinaryExpression binaryExpression)
        {
            var resultExpression = ConvertToUnaryExpression(binaryExpression);
            if (resultExpression is UnaryExpression)
            {
                return resultExpression;
            }

            return base.VisitBinary(binaryExpression);
        }

        /// <summary>
        /// Convert binary expression to unary. Return initial value if false
        /// </summary>
        /// <param name="binaryExpression">converted expression</param>
        /// <returns></returns>
        private Expression ConvertToUnaryExpression(BinaryExpression binaryExpression)
        {
            if (binaryExpression.Left.NodeType == ExpressionType.Parameter &&
                binaryExpression.Right.NodeType == ExpressionType.Constant)
            {
                var constValue = Expression.Constant(binaryExpression.Right).Value;
                int constPasreValue;
                if (int.TryParse(constValue.ToString(), out constPasreValue)
                    && constPasreValue == UnitValue)
                {
                    switch (binaryExpression.NodeType)
                    {
                        case ExpressionType.Add:
                            return Expression.Increment(binaryExpression.Left);
                        case ExpressionType.Subtract:
                            return Expression.Decrement(binaryExpression.Left);
                    }
                }
            }

            return binaryExpression;
        }
    }
}
