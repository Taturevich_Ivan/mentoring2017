﻿namespace AOPModule
{
    public class CallerDiagnosticObject
    {
        public string MethodName { get; set; }

        public string CallDate { get; set; }

        public double CallDuration { get; set; }

        public string[] Parameters { get; set; }

        public object ReturnValue { get; set; }

        public string ExceptionMessage { get; set; }
    }
}
