﻿namespace Module5_ImageScaner.Infrastructure.ReadModels
{
    public enum OutputType
    {
        Pdf = 0,

        Tiff = 1
    }
}
