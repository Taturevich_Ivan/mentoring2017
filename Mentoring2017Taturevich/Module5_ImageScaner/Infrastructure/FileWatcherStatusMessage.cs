﻿using System;
using Module5_ImageScaner.Infrastructure.ReadModels;
using System.Xml.Serialization;

namespace Module5_ImageScaner.Infrastructure
{
    [Serializable]
    public class FileWatcherStatusMessage
    {
        public FileWatcherStatusMessage()
        {
        }

        public FileWatcherStatusMessage(
            string messageId,
            ImageScannerOptions imageScannerOptions,
            FileMessageWatcherStatus fileWatcherStatusMessage)
        {
            MessageId = messageId;
            ImageScannerOptions = imageScannerOptions;
            FileMessageWatcherStatus = fileWatcherStatusMessage;
        }

        public string MessageId { get; set; }

        [XmlElement(nameof(ImageScannerOptions))]
        public ImageScannerOptions ImageScannerOptions { get; set; }

        public FileMessageWatcherStatus FileMessageWatcherStatus { get; set; }
    }
}
