﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;

namespace Module8_1
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine("Enter password:");
            var watch = new Stopwatch();
            var password = Console.ReadLine();
            var salt = GetSalt(16);
            watch.Start();
            var result = GeneratePasswordHashUsingSalt(password, salt);
            watch.Stop();
            Console.WriteLine($"Before optimization: {watch.ElapsedMilliseconds}ms");
            Console.WriteLine(result);
            watch.Reset();
            watch.Start();
            var newresult = GeneratePasswordHashUsingSaltOldVersion(password, salt);
            watch.Stop();
            Console.WriteLine($"After optimization using obsolete method: {watch.ElapsedMilliseconds}ms");
            Console.WriteLine(newresult);
            Console.ReadLine();
        }

        private static string GeneratePasswordHashUsingSalt(string passwordText, byte[] salt)
        {
            var iterate = 10000;
            var pbkdf2 = new Rfc2898DeriveBytes(passwordText, salt, iterate);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            var passwordHash = Convert.ToBase64String(hashBytes);
            return passwordHash;
        }

        private static string GeneratePasswordHashUsingSaltOldVersion(string passwordText, byte[] salt)
        {
            var iterate = 10000;
            var pbkdf1 = new PasswordDeriveBytes(passwordText, salt) { IterationCount = iterate };
            byte[] hash = pbkdf1.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            var passwordHash = Convert.ToBase64String(hashBytes);
            return passwordHash;
        }

        private static byte[] GetSalt(int maximumSaltLength)
        {
            var salt = new byte[maximumSaltLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }

            return salt;
        }
    }
}
