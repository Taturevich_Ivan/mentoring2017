﻿using System.Linq;
using System.Runtime.InteropServices;
using Module3_1;

namespace Module3_2
{
    [ComVisible(true)]
    [Guid("0F7D9952-6D4E-4783-B770-E9D7C9A9953B")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComDefaultInterface(typeof(ISystemPowerInformationComObject))]
    public class SystemPowerInformationComObject : ISystemPowerInformationComObject
    {
        private readonly SystemPowerInformation _systemPowerInformation;

        public SystemPowerInformationComObject()
        {
            _systemPowerInformation = new SystemPowerInformation();
        }

        public string GetPowerInformation()
        {
            var result = _systemPowerInformation.GetPowerInformation();

            return $"Power information: CoolingMode: {result.CoolingMode}," +
                   $" Idleness: {result.Idleness}, " +
                   $" Max Idleness Allowed: {result.MaxIdlenessAllowed}," +
                   $" Time Remaining {result.TimeRemaining}";
        }

        public string GetBatteryState()
        {
            var result = _systemPowerInformation.GetBatteryState();

            return $"BatteryState: Max Capacity {result.MaxCapacity}," +
                   $" Rate: {result.Rate}," +
                   $" Remaining Capacity: {result.RemainingCapacity}," +
                   $" Charging: {result.Charging}, " +
                   $" Discharging: {result.Discharging}";
        }

        public string GetLastSleepTime()
        {
            var result = _systemPowerInformation.GetLastSleepTime();

            return $"Last Sleep Time: {result}";
        }

        public string GetLastWakeTime()
        {
            var result = _systemPowerInformation.GetLastWakeTime();

            return $"Last Wake Time: {result}";
        }

        public void SetSuspendState(bool isHibernate)
        {
            _systemPowerInformation.SetSuspendState(isHibernate);
        }

        public void ReserveHibernationFile(bool isReserve)
        {
            _systemPowerInformation.ReserveHibernationFile(isReserve);
        }

        public string GetIdentity()
        {
            var guid = GetType()
                .GetCustomAttributes(typeof(GuidAttribute), true)
                .FirstOrDefault() as GuidAttribute;

            return guid != null ? guid.Value : ToString();
        }
    }
}
