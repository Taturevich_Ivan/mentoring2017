﻿using System;

namespace Module3_1
{
    static class Program
    {
        /// <summary>
        /// Check our unmanaged API
        /// </summary>
        static void Main()
        {
            var testApi = new SystemPowerInformation();

            var powerInformation = testApi.GetPowerInformation();
            Console.WriteLine($"Power information ID:{powerInformation.Idleness}");
            var batteryStateInformation = testApi.GetBatteryState();
            Console.WriteLine($"Battery state capacity:{batteryStateInformation.MaxCapacity}");
            var sleepTime = testApi.GetLastSleepTime();
            Console.WriteLine($"Last sleep time:{sleepTime}");
            var wakeTime = testApi.GetLastWakeTime();
            Console.WriteLine($"Last wake time:{wakeTime}");

            // Uncomment this for try to store hiberfil.sys. On my PC its about 6GB size
            // testApi.ReserveHibernationFile(true);

            // WARNING!) Uncomment this for try shut down PC to hibernation
            // testApi.SetSuspendState(true);
            Console.ReadKey();
        }
    }
}
