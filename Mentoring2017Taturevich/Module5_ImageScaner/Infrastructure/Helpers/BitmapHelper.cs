﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Encoder = System.Drawing.Imaging.Encoder;

namespace Module5_ImageScaner.Infrastructure.Helpers
{
    public interface IBitmapHelper
    {
        void CreateTiffImageFromUsedFileNames(List<string> fileNames, string outputFileDirectory);

        void SaveTiffByByteArray(byte[] bytes, string outputDirectory);

        byte[] CreateByteTiffImageFromUsedFileNames(List<string> fileNames);
    }

    public class BitmapHelper : IBitmapHelper
    {
        public void CreateTiffImageFromUsedFileNames(List<string> fileNames, string outputFileDirectory)
        {
            var initImageName = fileNames.FirstOrDefault();
            if (initImageName != null)
            {
                fileNames.Remove(initImageName);
                var tiff = GetTiffImageInMemoryStream(initImageName);
                var encoderInfo = ImageCodecInfo
                    .GetImageEncoders()
                    .First(i => i.MimeType == "image/tiff");
                var encoderParams = GetMultiFrameSaveEncoderParameters();
                tiff.Save(Path.Combine(outputFileDirectory,
                    $"{Guid.NewGuid()}.{ImageFormat.Tiff}"), encoderInfo, encoderParams);
                foreach (var imageName in fileNames)
                {
                    var nextImage = GetTiffImageInMemoryStream(imageName);
                    var addedItemParameters = GetFrameDimensionalEncoderParameters();
                    tiff.SaveAdd(nextImage, addedItemParameters);
                }

                var mainEncoderParams = GetFlushSavEncoderParameters();
                tiff.SaveAdd(mainEncoderParams);
                tiff.Dispose();
            }
        }

        public void SaveTiffByByteArray(byte[] bytes, string outputDirectory)

        {
            var fileName = GetFilePath(outputDirectory);
            using (var fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                fileStream.Write(bytes, 0, bytes.Length);
            }
        }

        public byte[] CreateByteTiffImageFromUsedFileNames(List<string> fileNames)
        {
            var initImageName = fileNames.FirstOrDefault();
            if (initImageName != null)
            {
                fileNames.Remove(initImageName);
                var tiff = GetTiffImageInMemoryStream(initImageName);
                var encoderInfo = ImageCodecInfo
                    .GetImageEncoders()
                    .First(i => i.MimeType == "image/tiff");
                var encoderParams = GetMultiFrameSaveEncoderParameters();
                using (var memoryStream = new MemoryStream())
                {
                    tiff.Save(memoryStream, encoderInfo, encoderParams);
                    foreach (var imageName in fileNames)
                    {
                        var nextImage = GetTiffImageInMemoryStream(imageName);
                        var addedItemParameters = GetFrameDimensionalEncoderParameters();
                        tiff.SaveAdd(nextImage, addedItemParameters);
                    }

                    var mainEncoderParams = GetFlushSavEncoderParameters();
                    tiff.SaveAdd(mainEncoderParams);
                    tiff.Dispose();

                    return memoryStream.ToArray();
                }
            }

            return Array.Empty<byte>();
        }

        private Image GetTiffImageInMemoryStream(string fileName)
        {
            var byteStream = GetImageMemoryStream(fileName);
            var tiff = Image.FromStream(byteStream);

            return tiff;
        }

        private MemoryStream GetImageMemoryStream(string fileName)
        {
            var bitmap = (Bitmap)Image.FromFile(fileName);
            var byteStream = new MemoryStream();
            bitmap.Save(byteStream, ImageFormat.Tiff);

            return byteStream;
        }

        private EncoderParameters GetMultiFrameSaveEncoderParameters()
        {
            var encoderParams = new EncoderParameters(2);
            var parameter = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionNone);
            encoderParams.Param[0] = parameter;
            parameter = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
            encoderParams.Param[1] = parameter;

            return encoderParams;
        }

        private EncoderParameters GetFrameDimensionalEncoderParameters()
        {
            var addedItemParameters = new EncoderParameters(2);
            var saveEncodeParam =
                new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
            var compressionEncodeParam = new EncoderParameter(Encoder.Compression, (long)EncoderValue.CompressionNone);
            addedItemParameters.Param[0] = compressionEncodeParam;
            addedItemParameters.Param[1] = saveEncodeParam;

            return addedItemParameters;
        }

        private EncoderParameters GetFlushSavEncoderParameters()
        {
            var mainSaveEncodeParam = new EncoderParameter(
                Encoder.SaveFlag, (long)EncoderValue.Flush);
            var mainEncoderParams = new EncoderParameters(1)
            {
                Param = { [0] = mainSaveEncodeParam }
            };

            return mainEncoderParams;
        }

        private string GetFilePath(string outputDirectory)
        {
            return Path.Combine(outputDirectory, $"{Guid.NewGuid()}.tiff");
        }
    }
}
