﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Ninject.Extensions.Interception;

namespace AOPModule.Proxy
{
    public class SerializationInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            var callerObject = new CallerDiagnosticObject();

            var watch = new Stopwatch();
            watch.Start();
            invocation.Proceed();
            watch.Stop();

            var fileRootPath = Path.GetFullPath(
                Path.Combine(Directory.GetCurrentDirectory(),
                @"..\..\..\APPLICATION_LOGS_DynamicProxy.json"));

            callerObject.CallDuration = watch.ElapsedMilliseconds;
            callerObject.CallDate = DateTime.Now.ToShortDateString();
            callerObject.MethodName = invocation.Request.Method.Name;
            callerObject.Parameters = invocation.Request.Arguments.Select(x => GetType().ToString()).ToArray();
            callerObject.ReturnValue = invocation.ReturnValue;
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "APPLICATION_LOGS_DynamicProxy.json");
            try
            {
                var serializedObject = JsonConvert.SerializeObject(callerObject);
                File.AppendAllText(filePath, serializedObject);
                File.AppendAllText(fileRootPath, serializedObject);
            }
            catch (Exception)
            {
                File.AppendAllText(filePath, "Not Serializable");
                File.AppendAllText(fileRootPath, "Not Serializable");
            }
        }
    }
}
