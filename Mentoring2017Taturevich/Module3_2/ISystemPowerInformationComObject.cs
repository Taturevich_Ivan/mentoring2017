﻿using System.Runtime.InteropServices;

namespace Module3_2
{
    [Guid("B3FCBB92-78BA-4E90-8F5A-4C316EE191EC")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    [ComVisible(true)]
    public interface ISystemPowerInformationComObject
    {
        string GetPowerInformation();

        string GetBatteryState();

        string GetLastSleepTime();

        string GetLastWakeTime();

        void SetSuspendState(bool isHibernate);

        void ReserveHibernationFile(bool isReserve);

        string GetIdentity();
    }
}
