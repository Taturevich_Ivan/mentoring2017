﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Module5_ImageScaner.Infrastructure
{
    public static class BytesArrayExtensions
    {
        public static byte[] Compress(this byte[] currentBytes)
        {
            var output = new MemoryStream();
            using (var dstream = new DeflateStream(output, CompressionLevel.Optimal))
            {
                dstream.Write(currentBytes, 0, currentBytes.Length);
            }

            return output.ToArray();
        }

        public static byte[] Decompress(this byte[] currentBytes)
        {
            var input = new MemoryStream(currentBytes);
            var output = new MemoryStream();
            using (var dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }

            return output.ToArray();
        }

        public static IEnumerable<byte[]> Split(this byte[] value, int bufferLength)
        {
            var countOfArray = value.Length / bufferLength;
            if (value.Length % bufferLength > 0)
            {
                countOfArray++;
            }

            for (var i = 0; i < countOfArray; i++)
            {
                yield return value
                    .Skip(i * bufferLength)
                    .Take(bufferLength)
                    .ToArray();
            }
        }

        public static byte[] Combine(this byte[] currentArray, byte[] combinedArray)
        {
            var resultArray = new byte[currentArray.Length + combinedArray.Length];
            Buffer.BlockCopy(currentArray, 0, resultArray, 0, currentArray.Length);
            Buffer.BlockCopy(combinedArray, 0, resultArray, currentArray.Length, combinedArray.Length);

            return resultArray;
        }
    }
}
