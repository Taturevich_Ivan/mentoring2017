﻿namespace Module3_1.InfoClasses
{
    public class BatteryState
    {
        public bool AcOnLine { get; set; }
        public bool BatteryPresent { get; set; }
        public bool Charging { get; set; }
        public bool Discharging { get; set; }
        public byte Spare0 { get; set; }
        public byte Spare1 { get; set; }
        public byte Spare2 { get; set; }
        public byte Spare3 { get; set; }
        public uint MaxCapacity { get; set; }
        public uint RemainingCapacity { get; set; }
        public int Rate { get; set; }
        public uint EstimatedTime { get; set; }
        public uint DefaultAlert1 { get; set; }
        public uint DefaultAlert2 { get; set; }
    }
}
