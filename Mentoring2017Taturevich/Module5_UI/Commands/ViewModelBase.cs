﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Module5_UI.Properties;

namespace Module5_UI.Commands
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        private bool _isProcessed;
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }

            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public bool IsProcessed
        {
            get { return _isProcessed; }
            set { SetField(ref _isProcessed, value); }
        }

        /// <summary>
        /// Wrapper for async operations with busy indicator
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        protected async Task RunAsync(Action action)
        {
            IsProcessed = true;
            await Task.Run(action);
            IsProcessed = false;
        }

        /// <summary>
        /// Wrapper for old dialogs calls, when they can called not in UI thread
        /// </summary>
        /// <param name="action"></param>
        protected void RunInAppartmentThread(Action action)
        {
            var thread = new Thread(_ => action());
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();
        }
    }
}
