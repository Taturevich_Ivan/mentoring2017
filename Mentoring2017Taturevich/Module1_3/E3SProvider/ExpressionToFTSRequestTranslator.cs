﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Sample03.E3SClient.Entities;

namespace Sample03.E3SProvider
{
    public class ExpressionToFtsRequestTranslator : ExpressionVisitor
    {
        private StringBuilder _resultString;

        public string Translate(Expression exp)
        {
            _resultString = new StringBuilder();
            Visit(exp);

            return _resultString.ToString();
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.DeclaringType == typeof(Queryable)
                && node.Method.Name == QueryConstants.WhereStatememt)
            {
                var predicate = node.Arguments[1];
                Visit(predicate);

                return node;
            }

            if (node.Method.ReturnType == typeof(bool))
            {
                switch (node.Method.Name)
                {
                    case nameof(string.StartsWith):
                        Visit(node.Object);
                        var startArgument = node.Arguments[0];
                        WrapInBrackets(() =>
                        {
                            Visit(startArgument);
                            _resultString.Append(QueryConstants.StarSymbol);
                        });

                        return node;
                    case nameof(string.EndsWith):
                        Visit(node.Object);
                        var endArgument = node.Arguments[0];
                        WrapInBrackets(() =>
                        {
                            _resultString.Append(QueryConstants.StarSymbol);
                            Visit(endArgument);
                        });

                        return node;
                    case nameof(string.Contains):
                        Visit(node.Object);
                        var containsArgument = node.Arguments[0];
                        WrapInBrackets(() =>
                        {
                            _resultString.Append(QueryConstants.StarSymbol);
                            Visit(containsArgument);
                            _resultString.Append(QueryConstants.StarSymbol);
                        });

                        return node;
                }
            }
            return base.VisitMethodCall(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (node.NodeType)
            {
                case ExpressionType.Equal:
                    if (node.Left.NodeType == ExpressionType.MemberAccess
                        && node.Right.NodeType == ExpressionType.Constant)
                    {
                        WrapInBrackets(() => { Visit(node.Left); Visit(node.Right); });
                    }

                    if (node.Left.NodeType == ExpressionType.Constant
                        && node.Right.NodeType == ExpressionType.MemberAccess)
                    {
                        WrapInBrackets(() => { Visit(node.Right); Visit(node.Left); });
                    }

                    break;
                case ExpressionType.AndAlso:
                case ExpressionType.And:
                    Visit(node.Left);
                    _resultString.Append(QueryConstants.AndSeparator);
                    Visit(node.Right);
                    break;
                default:
                    throw new NotSupportedException($"Operation {node.NodeType} is not supported");
            }

            return node;
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            _resultString.Append(node.Member.Name).Append(QueryConstants.Colon);

            return base.VisitMember(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            _resultString.Append(node.Value);

            return node;
        }

        private void WrapInBrackets(Action action)
        {
            _resultString.Append(QueryConstants.LeftBracket);
            action.Invoke();
            _resultString.Append(QueryConstants.RightBracket);
        }
    }
}
