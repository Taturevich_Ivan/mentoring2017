﻿using System;
using System.Collections.Generic;
using System.Text;
using Module5_ImageScaner.Infrastructure.Helpers;
using Module5_ImageScaner.Infrastructure.ReadModels;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Module5_ImageScaner.Infrastructure
{
    public interface IFileSenderMessageService : IDisposable
    {
        EventingBasicConsumer Consumer { get; }

        void StartConsumeSettings();

        void SendTypeMessage(string message, string typeName);

        void SendMessageStatus(FileWatcherStatusMessage message);

        void SendFileMessage(byte[] fileByteArray, string fileSaveDirectory, OutputType outputType);
    }

    public class FileSenderMessageService : IFileSenderMessageService
    {
        private readonly IModel _model;
        private readonly IConnection _connection;

        public FileSenderMessageService()
        {
            var factory = new ConnectionFactory
            {
                UserName = "guest",
                Password = "guest",
                HostName = "localhost",
                RequestedHeartbeat = 30,
            };
            _connection = factory.CreateConnection();
            _model = _connection.CreateModel();
            _model.QueueDeclare(queue: "Mentoring2017SettingsQueue",
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            SendMessage("Client connected");
            Consumer = new EventingBasicConsumer(_model);
        }

        public EventingBasicConsumer Consumer { get; }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _model.Close();
                _connection.Close();
            }
        }

        public void StartConsumeSettings()
        {
            _model.BasicConsume("Mentoring2017SettingsQueue", true, Consumer);
        }

        public void SendMessageStatus(FileWatcherStatusMessage message)
        {
            var basicProperties = _model.CreateBasicProperties();
            basicProperties.Persistent = true;
            basicProperties.ContentType = "text/xml";
            basicProperties.Type = typeof(FileWatcherStatusMessage).Name;
            var bytes = ImageScannerSerializer.SerializeStatusMessageToBytesViaXml(message);
            _model.BasicPublish("", "Mentoring2017FileQueue", basicProperties, bytes);
        }

        public void SendFileMessage(byte[] fileByteArray, string fileSaveDirectory, OutputType outputType)
        {
            var basicProperties = _model.CreateBasicProperties();
            basicProperties.Persistent = true;
            basicProperties.ContentType = "application/octet-stream";
            PublishInChunks(basicProperties, fileByteArray, fileSaveDirectory, outputType);
        }

        private void SendMessage(string message)
        {
            var basicProperties = _model.CreateBasicProperties();
            basicProperties.Persistent = true;
            var bytes = Encoding.UTF8.GetBytes(message);
            _model.BasicPublish("", "Mentoring2017FileQueue", basicProperties, bytes);
        }

        public void SendTypeMessage(string message, string typeName)
        {
            var basicProperties = _model.CreateBasicProperties();
            basicProperties.Headers = new Dictionary<string, object>
            {
                {"SettingsChanged", true}
            };
            basicProperties.Persistent = true;
            basicProperties.Type = typeName;
            var bytes = Encoding.UTF8.GetBytes(message);
            _model.BasicPublish("", "Mentoring2017SettingsQueue", basicProperties, bytes);
        }

        private void PublishInChunks(
            IBasicProperties basicProperties,
            byte[] bytes,
            string fileSaveDirectory, OutputType outputType)
        {
            const int chunkSize = 4096;
            bytes = bytes.Compress();
            var chunksCountMod = (int)Math.Truncate((double)bytes.Length / chunkSize);
            var headersDictionary = new Dictionary<string, object>
            {
                { nameof(HeadersModel.SaveFilePath), fileSaveDirectory}
            };
            if (chunksCountMod > 1)
            {
                headersDictionary.Add(nameof(HeadersModel.FileId), Guid.NewGuid().ToString());
                headersDictionary.Add(nameof(HeadersModel.IsChunked), true);
                headersDictionary.Add(nameof(HeadersModel.ChunkCount), chunksCountMod);
                headersDictionary.Add(nameof(HeadersModel.OutputType), outputType.ToString());

                var chunkNumber = 0;
                headersDictionary.Add(nameof(HeadersModel.IsComplete), false);
                foreach (var chunk in bytes.Split(chunkSize))
                {
                    if (chunkNumber == chunksCountMod)
                    {
                        headersDictionary[nameof(HeadersModel.IsComplete)] = true;
                    }

                    basicProperties.Headers = headersDictionary;
                    _model.BasicPublish("", "Mentoring2017FileQueue", basicProperties, chunk);
                    chunkNumber++;
                }
            }
            else
            {
                basicProperties.Headers = headersDictionary;
                headersDictionary.Add(nameof(HeadersModel.IsChunked), false);
                _model.BasicPublish("", "Mentoring2017FileQueue", basicProperties, bytes);
            }
        }
    }
}
