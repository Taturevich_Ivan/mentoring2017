﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Module4_Shared.Workers
{
    public class WhoisParser
    {
        public string GetInformationByDomainName(string domainName)
        {
            var html = string.Empty;
            var url = "https://www.iana.org/whois?q=" + domainName;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
                if (stream != null)
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        html = reader.ReadToEnd();
                    }
                }

            var info = ExtractInfoFromHtmlString(html);
            return info;
        }

        public async Task<string> GetInformationByDomainNameAsync(string domainName)
        {
            var html = string.Empty;
            var url = "https://www.iana.org/whois?q=" + domainName;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync().ConfigureAwait(false))
            using (Stream stream = response.GetResponseStream())
                if (stream != null)
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        html = await reader.ReadToEndAsync().ConfigureAwait(false);
                    }
                }

            var info = ExtractInfoFromHtmlString(html);
            return info;
        }

        private static string ExtractInfoFromHtmlString(string htmlString)
        {
            var startTag = @"<p id=""results"">" + "\n<pre>";
            var startIndex = htmlString.IndexOf(startTag, StringComparison.Ordinal) + startTag.Length;
            var endIndex = htmlString.IndexOf("</pre>", startIndex, StringComparison.Ordinal);
            var text = htmlString.Substring(startIndex, endIndex - startIndex);

            return text;
        }
    }
}
