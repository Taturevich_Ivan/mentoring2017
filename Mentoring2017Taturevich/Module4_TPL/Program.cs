﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Module4_Shared.Workers;

namespace Module4_TPL
{
    static class Program
    {
        private static int _progressCount;
        private static WhoisParser Parser { get; } = new WhoisParser();
        private static List<float> CpuCountList { get; } = new List<float>();
        private static DomainFileHelper FileHelper { get; } = new DomainFileHelper();

        static readonly ManualResetEventSlim ResetEvent = new ManualResetEventSlim(true);

        private static PerformanceCounter CpuCounter { get; } = new PerformanceCounter("Processor", "% Processor Time", "_Total");

        static void Main()
        {
            var watcher = new Stopwatch();
            var taskList = new List<Task>();
            var tokenSource = new CancellationTokenSource();
            watcher.Start();
            using (var locator = new IanaPageObject())
            {
                Console.Clear();
                Console.WriteLine("TPL test started...");
                Console.WriteLine("To pause process press P button");
                Console.WriteLine("To resume process press Enter button");
                Console.CursorVisible = false;
                var domains = locator.GetDomainsElementList();

                new Task(() =>
                {
                    foreach (var domainName in locator.GetDomainsNameListFromElements(domains))
                    {
                        taskList.Add(Task.Factory.StartNew(() => ProcessDomainFile(new object[]
                        {
                            domainName, tokenSource.Token
                        }), CancellationToken.None));
                    }
                    Task.WaitAll(taskList.ToArray());
                }).Start();

                // Info Task
                new Task(async () =>
                {
                    while (true)
                    {
                        if (_progressCount == DomainFileHelper.ResultFileCount)
                        {
                            watcher.Stop();
                            Console.WriteLine();
                            Console.WriteLine($"\nSpent time: {watcher.Elapsed}");
                            Console.WriteLine($"Average CPU usage: {CpuCountList.Average()}%");
                            Console.CursorVisible = true;
                            Console.WriteLine($"Directory with result files: \n{FileHelper.BaseDataFileFolder}");
                            Console.ReadLine();
                            break;
                        }

                        // To facilitate variable pooling
                        await Task.Delay(10, tokenSource.Token);
                    }
                }).Start();

                while (!Console.KeyAvailable)
                {
                    var readKey = Console.ReadKey(false).Key;
                    switch (readKey)
                    {
                        case ConsoleKey.P:
                            ResetEvent.Reset();
                            break;
                        case ConsoleKey.Enter:
                            ResetEvent.Set();
                            break;
                    }
                }
            }
        }

        private static void ProcessDomainFile(object domainName)
        {
            var array = domainName as object[];
            if (array != null)
            {
                var convertedName = array[0].ToString();
                var token = (CancellationToken)array[1];
                try
                {
                    ResetEvent.Wait(token);
                }
                catch (TaskCanceledException)
                {
                    Console.WriteLine("The operation was canceled.");
                    return;
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("The wait operation was canceled.");
                    return;
                }

                var domainInfoTask = Task.Run(() => Parser.GetInformationByDomainName(convertedName), token);
                domainInfoTask.ContinueWith(domainInfo => SafeToFile(new object[] { domainInfo.Result, convertedName }), token);
            }

        }

        private static void SafeToFile(object info)
        {
            var array = info as object[];
            if (array != null)
            {
                var text = array[0].ToString();
                var name = array[1].ToString();
                FileHelper.WriteDomainInfoFile(text, name);
                Interlocked.Increment(ref _progressCount);
                CpuCountList.Add(CpuCounter.NextValue());
                Console.Write($"Files created: {_progressCount}/{DomainFileHelper.ResultFileCount}." +
                              $" Threads in use: {Process.GetCurrentProcess().Threads.Count}\r");
            }
        }
    }
}
