﻿using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Module4_Shared.Workers
{
    public class DomainFileHelper
    {
        public const int ResultFileCount = 1563;

        public DomainFileHelper([CallerFilePath] string filePath = "")
        {
            var directory = Path.GetDirectoryName(filePath);
            if (directory != null)
            {
                BaseDataFileFolder = Directory
                    .CreateDirectory(Path.Combine(directory, "DomainInfoResults"))
                    .FullName;
            }
        }

        public string BaseDataFileFolder { get; }

        public void WriteDomainInfoFile(string domainInfo, string domainName)
        {
            var filePath = Path.Combine(BaseDataFileFolder, $"{domainName}.txt");
            File.WriteAllText(filePath, domainInfo);
        }

        public async Task WriteDomainInfoFileAsync(string domainInfo, string domainName)
        {
            var filePath = Path.Combine(BaseDataFileFolder, $"{domainName}.txt");
            using (var streamWriter = new StreamWriter(filePath))
            {
                await streamWriter.WriteAsync(domainInfo);
            }
        }

        public int GetStandartFilesCountInDomainDirectory()
        {
            return Directory.GetFiles(BaseDataFileFolder).Length;
        }
    }
}
