﻿using System;
using System.Collections.Generic;

namespace Module5_ImageScaner.Infrastructure.ReadModels
{
    [Serializable]
    public class ImageScannerOptions
    {
        public int DelayTime { get; set; }

        public string BarcodeText { get; set; }

        public string OutputFolder { get; set; }

        public OutputType OutputType { get; set; }

        public string TemporaryFolder { get; set; }

        public List<string> ObservedFolders { get; set; }
    }
}
