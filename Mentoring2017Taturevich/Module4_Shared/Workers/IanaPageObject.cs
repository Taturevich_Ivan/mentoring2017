﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;

namespace Module4_Shared.Workers
{
    public class IanaPageObject : IDisposable
    {
        private readonly IWebDriver _webDriver;

        public IanaPageObject()
        {
            _webDriver = new PhantomJSDriver();
            _webDriver.Navigate().GoToUrl("https://www.iana.org/domains/root/db");
        }

        public List<IWebElement> GetDomainsElementList()
        {
            var elements = _webDriver
                .FindElements(By.CssSelector("*[class='domain tld']"));

            return elements.ToList();
        }

        public IEnumerable<string> GetDomainsNameListFromElements(IEnumerable<IWebElement> webElements)
        {
            return webElements
                .Select(webElement => webElement
                    .FindElement(By.TagName("a")))
                .Select(link => link.Text);
        }

        /// <summary>
        /// Use this method in case of mess in processes
        /// </summary>
        public static void BrutalKillAllPhantomBrowsers()
        {
            foreach (var proc in Process.GetProcessesByName("PhantomJS"))
            {
                proc.Kill();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                _webDriver?.Quit();
            }
        }
    }
}

