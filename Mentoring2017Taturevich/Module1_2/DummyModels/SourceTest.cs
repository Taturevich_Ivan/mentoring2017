﻿using System;
using System.Collections.Generic;

namespace Module1_2.DummyModels
{
    public class SourceTest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime Date { get; set; }

        public List<string> AddressList { get; set; }
    }
}
