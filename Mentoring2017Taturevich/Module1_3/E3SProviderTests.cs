﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sample03.E3SClient.Entities;
using Sample03.E3SClient;
using System.Configuration;
using System.Linq;
using Sample03.E3SProvider;

namespace Sample03
{
    [TestClass]
    public class E3SProviderTests
    {
        [TestMethod]
        public void WithoutProvider()
        {
            // Provide you EPAM credentials here
            var client = new E3SQueryClient(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);
            var res = client.SearchFTS<EmployeeEntity>("workstation:(EPRUIZHW0249)", 0, 1);
            foreach (var emp in res)
            {
                Console.WriteLine("{0} {1}", emp.nativename, emp.startworkdate);
            }
        }

        [TestMethod]
        public void WithoutProviderNonGeneric()
        {
            // Provide you EPAM credentials here
            var client = new E3SQueryClient(ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["password"]);
            var res = client.SearchFTS(typeof(EmployeeEntity), "workstation:(EPRUIZHW0249)", 0, 10);

            foreach (var emp in res.OfType<EmployeeEntity>())
            {
                Console.WriteLine("{0} {1}", emp.nativename, emp.startworkdate);
            }
        }


        [TestMethod]
        public void WithProvider()
        {
            // Provide you EPAM credentials here
            var employees = new E3SEntitySet<EmployeeEntity>("epam_email", "epam_password");

            Console.WriteLine("\nReverse operand order test:");
            foreach (var emp in employees.Where(e => "EPBYGOMW0559" == e.workstation))
            {
                Console.WriteLine("{0} {1}", emp.nativename, emp.startworkdate);
            }
            Console.WriteLine("\nStartsWith query test:");
            foreach (var emp in employees.Where(e => e.workstation.StartsWith("EPBYGOM")))
            {
                Console.WriteLine("{0} {1}", emp.nativename, emp.startworkdate);
            }
            Console.WriteLine("\nEndsWith query test:");
            foreach (var emp in employees.Where(e => e.workstation.EndsWith("10")))
            {
                Console.WriteLine("{0} {1}", emp.nativename, emp.startworkdate);
            }
            Console.WriteLine("\nContains query test:");
            foreach (var emp in employees.Where(e => e.workstation.Contains("EPBYGOMW0559")))
            {
                Console.WriteLine("{0} {1}", emp.nativename, emp.startworkdate);
            }
            Console.WriteLine("\nAND statement test:");
            foreach (var emp in employees.Where(e => e.workstation.StartsWith("EPBYGOMW") && e.nativename.Contains("Андрей")))
            {
                Console.WriteLine("{0} {1}", emp.nativename, emp.startworkdate);
            }
        }
    }
}
