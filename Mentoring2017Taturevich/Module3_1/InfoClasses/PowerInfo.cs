﻿namespace Module3_1.InfoClasses
{
    public class PowerInfo
    {
        public uint MaxIdlenessAllowed { get; set; }
        public uint Idleness { get; set; }
        public uint TimeRemaining { get; set; }
        public byte CoolingMode { get; set; }
    }
}
