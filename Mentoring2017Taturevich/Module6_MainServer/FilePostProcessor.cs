﻿using System;
using System.Collections.Concurrent;
using Module5_ImageScaner.Infrastructure;
using Module5_ImageScaner.Infrastructure.Helpers;
using Module5_ImageScaner.Infrastructure.ReadModels;

namespace Module6_MainServer
{
    public class FilePostProcessor
    {
        private readonly PdfHelper _pdfHelper = new PdfHelper();
        private readonly BitmapHelper _bitmapHelper = new BitmapHelper();
        private readonly ConcurrentDictionary<string, byte[]> _fileMessagesDictionary
            = new ConcurrentDictionary<string, byte[]>();

        public void StartPostProcessing(
            string clientId,
            byte[] fileMessageChunk,
            bool isCompeted,
            OutputType outputType,
            string saveDirectory)
        {

            if (_fileMessagesDictionary.ContainsKey(clientId))
            {
                byte[] clientPartialMessage;
                if (_fileMessagesDictionary.TryGetValue(clientId, out clientPartialMessage))
                {
                    var newFileMessage = clientPartialMessage.Combine(fileMessageChunk);
                    if (isCompeted)
                    {
                        SaveByteArrayToFile(newFileMessage, outputType, saveDirectory);
                        byte[] removedFile;
                        _fileMessagesDictionary.TryRemove(clientId, out removedFile);
                        Console.WriteLine($"File {clientId} was successfully processed" +
                                          $" and saved ({newFileMessage.Length} bytes)");
                    }
                    else
                    {
                        _fileMessagesDictionary
                            .TryUpdate(clientId, newFileMessage, clientPartialMessage);
                    }
                }
            }
            else
            {
                Console.WriteLine($"New chunked file {clientId} was received");
                _fileMessagesDictionary.TryAdd(clientId, fileMessageChunk);
            }
        }

        public void SaveByteArrayToFile(byte[] fileBytes, OutputType outputType, string outputPath)
        {
            fileBytes = fileBytes.Decompress();
            switch (outputType)
            {
                case OutputType.Pdf:
                    _pdfHelper.SavePdfByByteArray(fileBytes, outputPath);
                    break;
                case OutputType.Tiff:
                    _bitmapHelper.SaveTiffByByteArray(fileBytes, outputPath);
                    break;
            }
        }
    }
}
