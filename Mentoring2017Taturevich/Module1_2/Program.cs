﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Module1_2.DummyModels;

namespace Module1_2
{
    static class Program
    {
        static void Main()
        {
            var source = new SourceTest
            {
                Id = 1,
                Date = DateTime.Now,
                AddressList = new List<string>
                {
                    "Address1",
                    "Address2",
                    "Address3",
                    "Address4"
                },
                Name = "TestName"
            };
            ObjectInfoTrace(source);
            var mapper = MapperGenerator.CreateMapper<SourceTest, DestinationTest>();
            var mappingResult = mapper.Map(source);
            ObjectInfoTrace(mappingResult);
            Console.ReadKey();
        }

        static void ObjectInfoTrace(object obj)
        {
            Console.WriteLine($"Object of type {obj.GetType().Name} has properties:");
            foreach (PropertyDescriptor propertyDesc in TypeDescriptor.GetProperties(obj))
            {
                var name = propertyDesc.Name;
                var value = propertyDesc.GetValue(obj);
                Console.WriteLine("{0}={1}", name, value);
            }
            Console.WriteLine("______________________________________");
        }
    }
}
