﻿using System;
using System.Diagnostics;
using System.IO;
using Module3_2;

namespace Module3_3
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine("Press ESC to stop and cleanup");
            Console.WriteLine("Press 1 to execute SYSTEM POWER INFO script");
            Console.WriteLine("Press 2 to execute RESERVE HIBERNATION FILE script");
            Console.WriteLine("Press 3 to execute SYSTEM HIBERNATION script (it will shut down your computer)");
            while (!Console.KeyAvailable)
            {
                var readKey = Console.ReadKey(true).Key;
                if (readKey == ConsoleKey.Escape)
                {
                    break;
                }

                if (readKey == ConsoleKey.D1 || readKey == ConsoleKey.NumPad1)
                {
                    ExecuteScript("testCOMInformationScript.vbs");
                }

                if (readKey == ConsoleKey.D2 || readKey == ConsoleKey.NumPad2)
                {
                    ExecuteScript("testCOMReserveHibFileScript.vbs");
                }

                if (readKey == ConsoleKey.D3 || readKey == ConsoleKey.NumPad3)
                {
                    ExecuteScript("testCOMHibernationScript.vbs");
                }
            }
            var comObj = new SystemPowerInformationComObject();
            comObj.ReserveHibernationFile(false);
        }

        private static void ExecuteScript(string scriptName)
        {
            var directoryInfo = Directory
                .GetParent(Directory
                    .GetCurrentDirectory())
                .Parent;
            if (directoryInfo != null)
            {
                var fullTestScriptPath = Path.Combine(directoryInfo
                    .FullName, scriptName);
                Console.WriteLine("Test script processing... Don't close application");
                Process.Start($@"{fullTestScriptPath}");
            }
        }
    }
}
